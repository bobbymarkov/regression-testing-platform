export const createBoardTableQuery =
    `CREATE TABLE IF NOT EXISTS board (
    id serial CONSTRAINT PK_board PRIMARY KEY, 
    name VARCHAR(50) CONSTRAINT nn_board_name NOT NULL,
    due_date TIMESTAMP CONSTRAINT NN_board_creation_date NOT NULL
    )`;

export const createAccountTableQuery =
    `CREATE TABLE IF NOT EXISTS account (
    username VARCHAR(50) CONSTRAINT pk_account PRIMARY KEY,
    email VARCHAR(70) CONSTRAINT nn_account_email NOT NULL,
    password TEXT CONSTRAINT nn_account_password NOT NULL,
    is_admin BOOLEAN DEFAULT false CONSTRAINT nn_account_is_admin NOT NULL,
    change_pass_token VARCHAR
    )`;

export const createTestcaseTableQuery =
    `CREATE TABLE IF NOT EXISTS testcase (
    id SERIAL CONSTRAINT pk_testcase PRIMARY KEY,
    board_id INT CONSTRAINT nn_testcase_board_id NOT NULL,
    name_tester VARCHAR(50), 
    name VARCHAR(70) CONSTRAINT nn_testcase_name NOT NULL, 
    status VARCHAR(30) DEFAULT 'Not Started' CONSTRAINT nn_testcase_status NOT NULL,
    duration INTERVAL,
    due_date TIMESTAMP,
    weight INT,
    description TEXT,
    environment TEXT,
    CONSTRAINT fk_testcase_board FOREIGN KEY (board_id) REFERENCES board(id) ON DELETE CASCADE,
    CONSTRAINT fk_testcase_account FOREIGN KEY (name_tester) REFERENCES account(username) ON UPDATE CASCADE
    )`;

export const createProductTableQuery =
    `CREATE TABLE IF NOT EXISTS product (
    name VARCHAR(50) CONSTRAINT pk_product PRIMARY KEY
     )`;


export const createTestcaseProductTableQuery =
    `CREATE TABLE IF NOT EXISTS testcase_product (
    testcase_id INT CONSTRAINT nn_testcase_product_testcase_id NOT NULL,
    product_name VARCHAR(50) CONSTRAINT nn_testcase_product_product_name NOT NULL,
    CONSTRAINT pk_testcase_product PRIMARY KEY (testcase_id, product_name),
    CONSTRAINT fk_testcase_product_testcase FOREIGN KEY (testcase_id) REFERENCES testcase(id) ON DELETE CASCADE,
    CONSTRAINT fk_testcase_product_product FOREIGN KEY (product_name) REFERENCES product(name) ON DELETE CASCADE
     )`

export const createStepTableQuery =
    `CREATE TABLE IF NOT EXISTS step (
    id SERIAL CONSTRAINT pk_step PRIMARY KEY,
    testcase_id INT CONSTRAINT nn_step_testcase_id NOT NULL,
    description TEXT CONSTRAINT nn_step_description NOT NULL,
    status VARCHAR(30),
    CONSTRAINT fk_step_testcase FOREIGN KEY (testcase_id) REFERENCES testcase(id) ON DELETE CASCADE
    )`;

export const createBugTableQuery =
    `CREATE TABLE IF NOT EXISTS bug (
    id SERIAL CONSTRAINT pk_bug PRIMARY KEY,
    step_id INT CONSTRAINT nn_bug_step_id NOT NULL,
    name_tester VARCHAR(50) CONSTRAINT nn_bug_name_tester NOT NULL,
    title VARCHAR(50) CONSTRAINT nn_bug_name NOT NULL,
    description TEXT,
    environment TEXT,
    product VARCHAR(50) CONSTRAINT nn_product NOT NULL,
    blocking BOOLEAN DEFAULT false,
    status VARCHAR(30) DEFAULT 'Not Resolved',
    CONSTRAINT fk_bug_step FOREIGN KEY (step_id) REFERENCES step(id) ON DELETE CASCADE,
    CONSTRAINT fk_bug_account FOREIGN KEY (name_tester) REFERENCES account(username) ON DELETE CASCADE ON UPDATE CASCADE    )`;

export const createAttachmentQuery =
    `CREATE TABLE IF NOT EXISTS attachment (
    id SERIAL CONSTRAINT pk_attachment PRIMARY KEY,
    bug_id INT CONSTRAINT nn_attachment_bug_id NOT NULL,
    reference TEXT CONSTRAINT nn_attachment_reference NOT NULL,
    CONSTRAINT fk_attachment_bug FOREIGN KEY (bug_id) REFERENCES bug(id) ON DELETE CASCADE
    )`;