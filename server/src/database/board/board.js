import * as BoardQueries from "./boardQueries.js";
import client from "../helper/databaseHelper.js";


export async function getAllBoards() {
    const result = await client.query(BoardQueries.getAllBoards);
    return result.rows;
}

export async function getBoardsById(id) {
    const values = [id];
    const results = await client.query(BoardQueries.getBoardById, values);
    return results.rows[0];
}

export async function getBoardsByName(name) {
    const values = [name];
    await client.query(BoardQueries.getBoardByName, values);
}

export async function getAllTestcasesOfOneBoard(id) {
    const values = [id];
    const testcases = await client.query(BoardQueries.getTestCasesByBoardIdQuery, values)
    return testcases.rows;
}

export async function createBoard(name, due_date) {
    const values = [ name, due_date];
    await client.query(BoardQueries.createBoard, values);
}

export async function updateBoard(id,name, due_date) {
    const values = [name, due_date, id];
    console.log(name, due_date, id)
    await client.query(BoardQueries.updateBoard, values);
}

export async function deleteBoard(id){
    const values = [id];
    await client.query(BoardQueries.deleteBoard, values);
}

export async function duplicateBoard(name, due_date) {
    const values = [name, due_date];
    const result = await client.query(BoardQueries.duplicateBoardAndReturnId, values);
    return result.rows[0].id;
}
