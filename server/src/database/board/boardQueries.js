export const getAllBoards = `SELECT * FROM board`;
export const getBoardById = 'SELECT * FROM board WHERE id = $1;'
export const getBoardByName = 'SELECT * FROM board WHERE name = $1;'
export const getTestCasesByBoardIdQuery = 'SELECT * FROM testcase WHERE board_id = $1';

export const createBoard = 'INSERT INTO board ( name, due_date) VALUES ($1, $2);';
export const updateBoard =  'UPDATE board SET name = $1, due_date = $2 WHERE id = $3;';
export const deleteBoard = 'DELETE FROM board WHERE id = $1';

export const duplicateBoardAndReturnId = 'INSERT INTO board (name, due_date) VALUES ($1, $2 ) RETURNING id'
