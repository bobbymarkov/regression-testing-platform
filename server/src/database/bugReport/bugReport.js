import * as BugReportQueries from "./bugReportQueries.js";
import client from "../helper/databaseHelper.js";

export async function getAllBugReport() {
    const result = await client.query(BugReportQueries.getAllBugReports);
    return result.rows;
}

export async function getAllBugReportByBoardId(board_id) {
    const result = await client.query(BugReportQueries.getAllBugReportsByBoardID, [board_id]);
    return result.rows;
}
export async function getAllBugReportByTestcaseId(testcase_id) {
    const result = await client.query(BugReportQueries.getAllBugReportsByTestcaseID, [testcase_id]);
    return result.rows;
}
export async function getABugReportById(bugReport_id) {
    const result = await client.query(BugReportQueries.getABugReportByID, [bugReport_id]);
    return result.rows[0];
}

export async function addBugReport(step_id, name_tester, title, description, environment, blocking, product, status) {
    const result = await client.query(BugReportQueries.addNewBugReport, [step_id, name_tester, title, description, environment, blocking, product, status]);
    return result.rows[0];
}
export async function editBugReport(bugReport_id, title, description, blocking, status) {
    await client.query(BugReportQueries.updateBugReportById, [bugReport_id, title, description, blocking, status]);
    return getABugReportById(bugReport_id);
}