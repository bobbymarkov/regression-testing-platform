//step_id, name_tester, product, environment are fetched

export const getAllBugReports =
    `SELECT *
    FROM bug;`

export const getAllBugReportsByBoardID =
    `SELECT *
    FROM bug
    INNER JOIN step ON bug.step_id = step.id
    INNER JOIN testcase ON step.testcase_id = testcase.id
    WHERE testcase.board_id = $1;`

export const getAllBugReportsByTestcaseID = `
    SELECT bug.id AS bug_id, *
    FROM bug
    INNER JOIN step ON bug.step_id = step.id
    INNER JOIN testcase ON step.testcase_id = testcase.id
    WHERE testcase.id = $1;
`;

export const addNewBugReport =
    `INSERT INTO
    bug(step_id, name_tester, title, description, environment, blocking, product, status)
    VALUES($1, $2, $3, $4, $5, $6, $7, $8)
    RETURNING id;`

export const getABugReportByID = `
    SELECT bug.*, step.testcase_id AS testcase_id
    FROM bug
    INNER JOIN step ON bug.step_id = step.id
    WHERE bug.id = $1;
`;

export const updateBugReportById =
    `UPDATE bug
    SET
        title = $2,
        description = $3,
        blocking = $4,
        status = $5
    WHERE id = $1;`