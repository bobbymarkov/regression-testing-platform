import client from "../helper/databaseHelper.js";
import * as AttachmentQueries from "../attachment/attachmentQueries.js";

export async function addAnAttachment(bug_id, reference) {
    const result = await client.query(AttachmentQueries.addNewAttachment, [bug_id, reference]);
    return result.rows[0].id;
}

export async function getAttachmentsByBugId(bug_id) {
    const result = await client.query(AttachmentQueries.getAttachmentByBugId, [bug_id]);
    return result.rows;
}