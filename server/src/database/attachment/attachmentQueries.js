export const addNewAttachment =
    `INSERT INTO
    attachment(bug_id, reference)
    VALUES($1, $2)
    RETURNING id;`

export const getAttachmentByBugId =
    `SELECT *
    FROM attachment
    INNER JOIN bug ON attachment.bug_id = bug.id
    INNER JOIN step ON bug.step_id = step.id
    INNER JOIN testcase ON step.testcase_id = testcase.id
    WHERE bug.id = $1`
