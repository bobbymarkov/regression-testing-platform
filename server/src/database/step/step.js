import * as StepQueries from "./stepQueries.js";
import client from "../helper/databaseHelper.js";


export async function insertStep(description, status, testCaseId) {
    const result = await client.query(StepQueries.insertStep, [description, status, testCaseId]);
    return result.rows[0];
}

export async function updateStepById(description, status, testcaseId, id) {
    const result = await client.query(StepQueries.updateStepById, [description, status, testcaseId, id]);
    return result.rows[0];
}

export async function getStepsGivenTestCaseId(testcaseId) {
    const result = await client.query(StepQueries.getStepsGivenTestcaseId, [testcaseId]);
    return result.rows;
}

export async function getAllSteps() {
    const result = await client.query(StepQueries.getAllSteps);
    return result.rows;
}

export async function getStepById(id) {
    const result = await client.query(StepQueries.getStepGivenId, [id]);
    return result.rows[0];
}

export async function removeStepById(id) {
    const result = await client.query(StepQueries.removeStepById, [id]);
    return result.rows[0];
}