export const insertStep = "INSERT INTO step (description, status, testcase_id) VALUES ($1, $2, $3) RETURNING *;";
export const getAllSteps = "SELECT * FROM step;";
export const getStepGivenId = "SELECT * FROM step WHERE id = $1;";
export const getStepsGivenTestcaseId = "SELECT * FROM step WHERE testcase_id = $1;";
export const updateStepById = `
  UPDATE step
  SET
    description = $1,
    status = $2,
    testcase_id = $3
  WHERE id = $4
  RETURNING *;`;

export const removeStepById = "DELETE FROM step WHERE id = $1;";