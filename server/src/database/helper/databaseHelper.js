import pg from 'pg'
import * as TableQueries from "../tableQueries.js";
import 'dotenv/config';

const config = {
    user: process.env.DATABASE_USER,
    host: process.env.DATABASE_HOST,
    database: process.env.DATABASE,
    password: process.env.DATABASE_PASSWORD,
    port: process.env.DATABASE_PORT
}


const client = new pg.Client(config);
try {
    await client.connect();
} catch (e) {
    console.log(e);
}

try {
    await client.query(TableQueries.createBoardTableQuery);
    await client.query(TableQueries.createAccountTableQuery);
    await client.query(TableQueries.createTestcaseTableQuery);
    await client.query(TableQueries.createProductTableQuery);
    await client.query(TableQueries.createTestcaseProductTableQuery);
    await client.query(TableQueries.createStepTableQuery);
    await client.query(TableQueries.createBugTableQuery);
    await client.query(TableQueries.createAttachmentQuery);
} catch (e) {
    console.log(e);
}

export default client;



