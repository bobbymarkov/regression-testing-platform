import * as userHelper from './userQueries.js';
import client from "../helper/databaseHelper.js";

export async function getUserByUsername(username){
    const result = await client.query(userHelper.getUserByUsername, [username]);
    return result.rows[0];
}

export async function getAllUsers() {
    const accounts = await client.query(userHelper.getAllUsers);
    return accounts.rows;
}

export async function insertNewUser(parameters){
    const res = await client.query(userHelper.insertNewUser, parameters);
    return res.rows[0];
}

export async function updateUser(username, newUsername, is_admin){
    const values = [newUsername, is_admin, username];
    await client.query(userHelper.updateUser, values)
}

export async function getAllTesters(){
    const testers = await client.query(userHelper.getAllTesters);
    return testers.rows;
}

export async function insertChangePassToken(token, username){
    const values = [token, username];
    await client.query(userHelper.insertChangePassToken, values);
}

export async function updatePassword(password, username){
    const values = [password, username];
    await client.query(userHelper.changePassword, values);
}