export const getUserByUsername = 'SELECT * FROM account WHERE username = $1';
export const getAllUsers = `SELECT * FROM account`;
export const getAllTesters = `SELECT * FROM account WHERE is_admin = false`;

export const updateUser = `UPDATE account SET username = $1, is_admin = $2 WHERE username = $3`;

export const insertNewUser = `INSERT INTO account (username, email, password, is_admin) VALUES ($1, $2, $3, $4)`;

export const insertChangePassToken = `UPDATE account SET change_pass_token = $1 WHERE username = $2`;
export const changePassword = `UPDATE account SET password = $1 WHERE username = $2`;