export const insertTestCase = `INSERT INTO testcase (board_id, due_date, name, name_tester, weight, environment, description, status) VALUES ($1, $2, $3, $4, $5, $6, $7, $8) RETURNING id;`;
export const insertTestCaseProduct = `INSERT INTO testcase_product (testcase_id, product_name) VALUES ($1, $2);`;
export const getTestCasesGivenId =
    `SELECT tc.*, tp.product_name
    FROM testcase tc
    LEFT JOIN testcase_product tp ON tc.id = tp.testcase_id
    WHERE tc.id = $1;`

export const getAllRelationsBetweenTestcasesAndProducts =
    `SELECT * FROM testcase_product`;

export const getProductsGivenId = `SELECT * FROM testcase_product WHERE testcase_id = $1;`;

export const getAllTestCasesQuery = "SELECT * FROM testcase;"

export const updateTestCaseById = `UPDATE testcase SET 
    board_id = $1, 
    name_tester = $2,
    name = $3,
    status = $4,
    duration = $5,
    due_date = $6,
    weight = $7,
    description = $8,
    environment = $9
  WHERE id = $10`;

export const deleteTestCase = `DELETE FROM testcase WHERE id = $1`;
export const deleteProductByTestCase = `DELETE FROM testcase_product WHERE testcase_id = $1 AND product_name = $2`;
export const deleteAllProductsByTestCaseId = `DELETE FROM testcase_product WHERE testcase_id = $1`;
export const getTestsByTesterName = `SELECT * FROM testcase WHERE name_tester = $1 AND status != $2`;
export const getTestcasesByBoardId = 'SELECT * FROM testcase WHERE board_id = $1';
export const duplicateTestcaseAndReturnId ='INSERT INTO testcase (board_id, due_date, name, name_tester, weight, environment, description, status) VALUES ($1, $2 ) RETURNING id';