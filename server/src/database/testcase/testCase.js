import * as testCaseHelper from './testcaseQueries.js';
import client from "../helper/databaseHelper.js";
import * as BoardQueries from "../board/boardQueries.js";

export async function getTestsByTesterName (username){
    const result = await client.query(testCaseHelper.getTestsByTesterName, [username, 'Done']);
    return result.rows;
}

export async function getAllRelationsBetweenTestcasesAndProducts() {
    const result = await client.query(testCaseHelper.getAllRelationsBetweenTestcasesAndProducts);
    return result.rows;
}

export async function insertTestCase(board_id, due_date, name, name_tester, weight, environment, description, status) {
    const values = [board_id, due_date, name, name_tester, weight, environment, description, status];
    const result = await client.query(testCaseHelper.insertTestCase,values );
    return result.rows[0].id;
}

export async function insertTestCaseProduct(testcase_id, product_name){
    await client.query(testCaseHelper.insertTestCaseProduct, [testcase_id, product_name]);
}

export async function updateTestCase(boardId, name_tester, name, status, duration, dueDate, weight, description, environment, id) {
    await client.query(testCaseHelper.updateTestCaseById, [boardId, name_tester, name, status, duration, dueDate, weight, description, environment, id]);
}

export async function getTestCaseByGivenId(id) {
    const result = await client.query(testCaseHelper.getTestCasesGivenId, [id]);
    return result.rows[0];
}

export async function getProductsForTestcaseByGivenId(id){
    const result = await client.query(testCaseHelper.getProductsGivenId, [id]);
    return result.rows;
}
export async function getAllTestCases(){
    const result = await client.query(testCaseHelper.getAllTestCasesQuery);
    return result.rows;
}

export async function deleteTestCaseByGivenId(id){
    const result = await client.query(testCaseHelper.deleteTestCase, [id]);
    return result.rows[0];
}

export async function deleteProductIdByTestCaseId(testcaseId, productId){
    const result = await client.query(testCaseHelper.deleteProductByTestCase, [testcaseId, productId]);
    return result.rows[0];
}
export async function deleteProductsByTestCaseId(testcaseId){
    const result = await client.query(testCaseHelper.deleteAllProductsByTestCaseId, [testcaseId]);
    return result.rows[0];
}
export async function getTestCaseByBoardId (boardId){
    const result = await client.query(testCaseHelper.getTestcasesByBoardId, [boardId]);
    return result.rows[0];
}
export async function duplicateTestCases(board_id, due_date, name, name_tester, weight, environment, description, status){
    const values = [board_id, due_date, name, name_tester, weight, environment, description, status];
    const result = await client.query(testCaseHelper.duplicateTestcaseAndReturnId,values );
    return result.rows[0].id;
}