import multer from 'multer';
import multerS3 from 'multer-s3';
import AWS from 'aws-sdk';
import 'dotenv/config';

// Get variables from dotenv
AWS.config.update({
    accessKeyId: process.env.ACCESS_KEY_ID,
    secretAccessKey: process.env.SECRET_ACCESS_KEY,
    region: process.env.REGION,
});

// Create an S3 client
const s3 = new AWS.S3();

// Function to determine the content type based on file extension
const determineContentType = (req, file, cb) => {
    const contentTypeMap = {
        'svg': 'image/svg+xml',
        'jpeg': 'image/jpeg',
        'png': 'image/png',
        'gif': 'image/gif',
        'mp4': 'video/mp4'
    };

    // extract file extension
    const fileExtension = file.originalname.split('.').pop().toLowerCase();
    const contentType = contentTypeMap[fileExtension] || 'application/octet-stream';

    cb(null, contentType);
};

// configure Multer for file upload with custom content type determination
const upload = multer({
    storage: multerS3({
        s3: s3,
        bucket: 'projectclientbucket',
        key: function (req, file, cb) {
            cb(null, Date.now() + '-' + file.originalname);
        },
        contentType: determineContentType,
        metadata: function (req, file, cb) {
            cb(null, { fieldName: file.fieldname });
        },
    }),
});

export default upload;
