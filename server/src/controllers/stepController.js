import * as helper from '../database/step/step.js';
import statusCodes from "http-status-codes";

////GET
export async function getSteps(req, res) {
    const steps = await helper.getAllSteps();

    if (steps) {
        res.status(statusCodes.OK).json({msg: "Retrieved all steps successfully!", steps: steps});
    } else {
        res.status(statusCodes.NOT_FOUND).json({error: `Retrieving all steps failed!`});
    }
}

export async function getStepById(req, res) {
    const id = req.params.id;

    if (!id) {
        return res.status(400).json({msg: "Missing Id when fetching getStepById!"});
    }

    const step = await helper.getStepById(id);

    if (step) {
        res.status(statusCodes.OK).json({msg: `Retrieved step with id ${id} successfully!`, step: step});
    } else {
        res.status(statusCodes.NOT_FOUND).json({error: `Retrieved step with id ${id} failed!`});
    }
}


////POST
export async function addNewStep(req, res) {
    const step = req.body;

    if (!step)
        return res.status(statusCodes.BAD_REQUEST).json({msg: "Missing step information!"});

    let stepDescription;
    if (step.description) {
        stepDescription = step.description;
    } else {
        stepDescription = "Description placeholder";
    }
    const stepInserted = await helper.insertStep(stepDescription, "N/A", step.testCaseId);

    if (stepInserted)
        res.status(200).json({msg: "Add step successfully", step: stepInserted});
    else
        res.status(statusCodes.NOT_FOUND).json({error: `Adding new step failed!`});

}


////PUT
export async function editStepById(req, res) {
    const id = req.params.id;
    const description = req.body.description;
    const status = req.body.status;
    const testcaseId = req.body.testCaseId;

    if (!id) {
        return res.status(statusCodes.BAD_REQUEST).json({msg: "Missing step ID when EDITING step"});
    }

    const result = await helper.updateStepById(description, status, testcaseId, id);


    if (result)
        res.status(200).json({msg: "Edit step successfully", step: result});
    else
        res.status(statusCodes.NOT_FOUND).json({error: `Edit step failed!`});
}


////DELETE
export async function deleteStepById(req, res) {
    const id = req.params.id;

    if (!id) {
        return res.status(statusCodes.BAD_REQUEST).json({msg: "Missing step ID when DELETING!"});
    }

    await helper.removeStepById(id);

    res.status(200).json({msg: "Delete successfully!"});
}

