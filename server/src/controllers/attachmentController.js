import statusCodes from "http-status-codes";
import * as attachmentHelper from "../database/attachment/attachment.js"

export async function postAnAttachment(req, res) {
    if (!req.file) {
        return res.status(statusCodes.BAD_REQUEST).json({error: 'No file uploaded!'});
    }

    // req.file should now contain the file information, including its S3 URLggg
    const bugId = req.params.id;
    const referencePath = req.file.location;
    await attachmentHelper.addAnAttachment(bugId, referencePath);

    res.status(statusCodes.CREATED).json({msg: "File uploaded successfully!"});
}

export async function getAllAttachmentsByBugId(req, res) {
    res.json(await attachmentHelper.getAttachmentsByBugId(req.params.id));
}
