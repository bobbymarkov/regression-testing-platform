import * as bugreportHelper from '../database/bugReport/bugReport.js';
import statusCodes from "http-status-codes";
import {validateOrThrow} from "../utils/validator.js";

export async function getAllBugReports(req, res) {
    res.json(await bugreportHelper.getAllBugReport());
}

export async function getAllBugReportsByBoardId(req, res) {
    res.json(await bugreportHelper.getAllBugReportByBoardId(req.params.id));
}

export async function getAllBugReportsByTestcaseID(req, res) {
    res.json(await bugreportHelper.getAllBugReportByTestcaseId(req.params.id));
}

export async function getABugReportByID(req, res) {
    const result = await bugreportHelper.getABugReportById(req.params.id);

    if (!result) {
        return res.status(statusCodes.NOT_FOUND).json({error: `Bug report with ID ${req.params.id} not found!`});
    }

    return res.json(result);
}

export async function postABugReport(req, res) {
    const bugReport = req.body;
    const {
        step_id,
        name_tester,
        title,
        description,
        environment,
        blocking,
        product,
        status,
    } = bugReport;

    validateOrThrow(bugReport, 'bugreport');
    const newBugReport = await bugreportHelper.addBugReport(step_id, name_tester, title, description, environment, blocking, product, status);

    res.status(statusCodes.CREATED)
        .json(newBugReport);
}

export async function editABugReport(req, res) {
    const bugReportId = req.params.id;
    const bugReportToChange = await bugreportHelper.getABugReportById(bugReportId);

    if (!bugReportToChange) {
        return res.status(statusCodes.NOT_FOUND).json({error: `Bug report with ID ${bugReportId} not found!`});
    }
    const bugReportModifications = req.body;
    bugReportModifications.id = Number(bugReportId);
    validateOrThrow(bugReportModifications, 'bugreport');

    const newBugReport = await bugreportHelper.editBugReport(bugReportId, bugReportModifications.title, bugReportModifications.description, bugReportModifications.blocking, bugReportModifications.status);

    res.status(statusCodes.OK).json(newBugReport);
}

