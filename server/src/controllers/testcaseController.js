import statusCodes from 'http-status-codes';
import * as helper from '../database/testcase/testCase.js';
import {getStepsGivenTestCaseId, insertStep} from "../database/step/step.js";

export async function createTestCase(req, res) {
    const newTestCase = req.body;

    let id = await helper.insertTestCase(newTestCase.board_id, newTestCase.due_date, newTestCase.name, newTestCase.name_tester, newTestCase.weight,
        newTestCase.environment, newTestCase.description, newTestCase.status);
    if (newTestCase.product) {
        for (let i = 0; i < newTestCase.product.length; i++) {
            await helper.insertTestCaseProduct(id, newTestCase.product[i]);
        }
    }
    if (newTestCase.steps) {
        for (let i = 0; i < newTestCase.steps.length; i++) {
            await insertStep(newTestCase.steps[i].description, newTestCase.steps[i].status, id);
        }
    }
    res.status(statusCodes.CREATED).json(id);
}

export async function getAllTestCases(req, res) {
    const testCases = await helper.getAllTestCases();
    res.status(statusCodes.OK).json(testCases);
}

export async function getAllRelationsBetweenTestcasesAndProducts(req, res) {
    const testCases = await helper.getAllRelationsBetweenTestcasesAndProducts();
    res.status(statusCodes.OK).json(testCases);
}

export async function getTestCaseById(req, res) {
    const testCaseId = req.params.id;

    const testCase = await helper.getTestCaseByGivenId(testCaseId);

    if (testCase) {
        res.status(statusCodes.OK).json(testCase);
    } else {
        res.status(statusCodes.NOT_FOUND).json({error: 'Test case not found'});
    }
}

export async function getProductsByTestCaseId(req, res) {
    const testCaseId = req.params.id;

    const products = await helper.getProductsForTestcaseByGivenId(testCaseId);

    if (products) {
        res.status(statusCodes.OK).json(products);
    } else {
        res.status(statusCodes.NOT_FOUND).json({error: 'Test case not found'});
    }
}

export async function getStepsByTestCaseId(req, res) {
    const testCaseId = req.params.id;

    const stepsOfTestcase = await getStepsGivenTestCaseId(testCaseId);

    if (stepsOfTestcase) {
        res.status(statusCodes.OK).json(stepsOfTestcase);
    } else {
        res.status(statusCodes.NOT_FOUND).json({error: `Steps of testcaseId ${testCaseId} not found!`});
    }
}

export async function updateTestCaseUsingId(req, res) {
    const testCaseId = req.params.id;
    const updatedTestCaseData = req.body;
    const existingTestCase = await helper.getTestCaseByGivenId(testCaseId);

    if (!existingTestCase) {
        res.status(statusCodes.NOT_FOUND).json({error: 'Test case not found'});
        return;
    }

    await helper.updateTestCase(
        updatedTestCaseData.board_id,
        updatedTestCaseData.name_tester,
        updatedTestCaseData.name,
        updatedTestCaseData.status,
        updatedTestCaseData.duration,
        updatedTestCaseData.due_date,
        updatedTestCaseData.weight,
        updatedTestCaseData.description,
        updatedTestCaseData.environment,
        testCaseId
    );
    res.status(statusCodes.OK).json();

}

export async function deleteProductsUsingTestCaseId(req, res) {
    const testCaseId = req.params.id;
    const productId = req.params.product;

    await helper.deleteProductIdByTestCaseId(testCaseId, productId);
    res.status(statusCodes.NO_CONTENT).json({message: 'No content'});
}

export async function addProductsUsingTestCaseId(req, res) {
    const testcase_product = req.body;

    const {testcase_id, product_name} = testcase_product;
    await helper.insertTestCaseProduct(testcase_id, product_name);

    res.status(statusCodes.CREATED).json({message: 'Ok'});
}

export async function deleteTestCaseById(req, res) {
    const testCaseId = req.params.id;
    const existingTestCase = await helper.getTestCaseByGivenId(testCaseId);

    if (!existingTestCase) {
        res.status(statusCodes.NOT_FOUND).json({error: 'Test case not found'});
    } else {
        await helper.deleteProductsByTestCaseId(testCaseId);
        await helper.deleteTestCaseByGivenId(testCaseId);
        res.status(statusCodes.NO_CONTENT).json({message: 'No content'});
    }
}