import * as helper from '../database/board/board.js';
import httpStatus from 'http-status-codes';
import {getAllBugReportByBoardId} from "../database/bugReport/bugReport.js";
import {durationInSeconds} from "../utils/util.js";

// GET CALLS
export async function getAllBoards(req, res) {
    const boards = await helper.getAllBoards();
    res.json(boards);
}

export async function getBoardById(req, res) {
    const boardId = req.params.id;
    const parsedID = parseInt(boardId);

    const board = await helper.getBoardsById(parsedID);

    if (parsedID) {
        if (board) {
            res.status(httpStatus.OK).json(board);
        } else {
            res.status(httpStatus.NOT_FOUND).json({error: 'Board not found'});
        }
    } else {
        res.status(httpStatus.BAD_REQUEST).json({error: 'Board Id is missing'});
    }
}

export async function getBoardByName(req, res) {
    const name = req.params.name;

    const board = await helper.getBoardsByName(name);

    if (name) {
        if (board) {
            res.status(httpStatus.OK).json(board);
        } else {
            res.status(httpStatus.NOT_FOUND).json({error: 'Board not found'});
        }
    } else {
        res.status(httpStatus.BAD_REQUEST).json({error: 'Board name is missing'});
    }
}

export async function getAllTestcasesOfOneBoard(req, res) {
    const id = parseInt(req.params.id);
    let testcases = await helper.getAllTestcasesOfOneBoard(id);

    res.status(httpStatus.OK).json(testcases);
}

// PUT CALL

export const updateBoard = (req, res) => {
    const boardId = req.params.id;
    const parsedID = parseInt(boardId)

    const {newName, newDue_date} = req.body;

    if (!parsedID) {
        return res.status(httpStatus.BAD_REQUEST).json({error: 'Board Id is missing'});
    }

    const boardIndex = helper.getBoardsById(parsedID);

    //change that if it is not in database
    if (boardIndex === -1) {
        return res.status(httpStatus.NOT_FOUND).json({error: 'Board not found'});
    }
    const updatedBoard = helper.updateBoard(parsedID, newName, newDue_date);

    if (updatedBoard) {
        const check = helper.getBoardsById(parsedID);
        res.status(httpStatus.OK).json({message: 'Updated the board', check});

    } else {
        res.status(httpStatus.INTERNAL_SERVER_ERROR).json({error: 'Failed to update the board'});
    }
};


// POST CALL

export const createBoard = (req, res) => {
    const {name, due_date} = req.body;

    console.log(due_date);
    if (!name) {
        return res.status(httpStatus.BAD_REQUEST).json({error: 'Name is required'});
    }
    if (!due_date) {
        return res.status(httpStatus.BAD_REQUEST).json({error: 'Date is required'});
    }
    const newBoard = helper.createBoard(name, due_date);
    if (newBoard) {
        const check = helper.getBoardsByName(name);
        res.status(httpStatus.CREATED).json(check);
    }
};

export async function getBoardProgress(req, res) {
    const boardId = req.params.id;
    const testCases = await helper.getAllTestcasesOfOneBoard(boardId);
    const bugReport = await getAllBugReportByBoardId(boardId);

    let totalWeight = 0;
    let totalBugs = 0;
    let totalDuration = 0;

    testCases.forEach(testCase => {
        totalWeight += testCase.weight;
        totalBugs = bugReport.length;
        totalDuration += durationInSeconds(testCase.duration);
    });

    const progressData = {
        weight: totalWeight,
        bugs: totalBugs,
        duration: totalDuration,
    };

    res.status(httpStatus.OK).json(progressData);
}

//DELETE CALL

export const deleteBoard = (req, res) => {
    const boardId = req.params.id;
    const parsedID = parseInt(boardId);

    const deletedBoard = helper.deleteBoard(parsedID);

    if (parsedID) {
        if (deletedBoard) {
            return res.status(httpStatus.NO_CONTENT).json({message: 'Board deleted successfully'});
        } else {
            res.status(httpStatus.NOT_FOUND).json({error: 'Board not found'});
        }
    } else {
        return res.status(httpStatus.BAD_REQUEST).json({error: 'Board ID is missing'});
    }
}

export const duplicateBoard = async (req, res) => {
    const {name, due_date} = req.body;

    if (!name) {
        return res.status(httpStatus.BAD_REQUEST).json({error: 'Name is required'});
    }
    if (!due_date) {
        return res.status(httpStatus.BAD_REQUEST).json({error: 'Date is required'});
    }
    const newBoard = await helper.duplicateBoard(name, due_date);
    if (newBoard) {
        res.status(httpStatus.CREATED).json(newBoard)
    }
};