import statusCodes from "http-status-codes";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import jsonwebtoken from "jsonwebtoken";
import nodemailer from "nodemailer"
import * as userHelper from "../database/user/user.js";
import * as testHelper from "../database/testcase/testCase.js";

export const secret = 'fasdohafdshiafjledcbiaewjhrfasbia!@±!@±!#±aewkbiahj231421312412341fiewa';
const secretKey = '07123e1f482356c415f684407a3b8723e10b2cbbc0b8fcd6282c49d37c9c1abc';


export async function getAllAccounts(req, res) {
    const accounts = await userHelper.getAllUsers();

    res.status(statusCodes.OK).json(accounts);
}

export async function logInUser(req, res) {
    const {email, password} = req.body;

    if (!email || !password) {
        return res.status(statusCodes.BAD_REQUEST).json({message: 'Login failed'});
    }

    const user = await userHelper.getUserByUsername(email);
    if (!user) {
        return res.status(statusCodes.UNAUTHORIZED).json({message: 'Login failed'});
    }

    const passwordMatch = await bcrypt.compare(password, user.password);
    if (!passwordMatch) {
        return res.status(statusCodes.UNAUTHORIZED).json({message: 'Login failed'});
    }

    // Generate a JWT token
    const token = await generateToken(user.username, user.is_admin);


    res.status(statusCodes.CREATED).json({token: token});
}

export async function registerNewUser(req, res) {
    const username = req.body.usernameForCreating;
    const password = req.body.passwordForCreating;
    const is_admin = req.body.is_adminForCreating;
    const email = req.body.emailForCreating;

    if (!username || !password) {
        return res.status(statusCodes.BAD_REQUEST).json({message: 'Registration failed'});
    }
    if (password.length < 6 || password.length > 20) {
        return res.status(statusCodes.BAD_REQUEST).json({message: 'Registration failed'});
    } else {
        // Hash the password
        const passwordHash = await bcrypt.hash(password, 10);

        // Check if user already exists
        const existingUser = await userHelper.getUserByUsername(username);
        if (existingUser) {
            return res.status(statusCodes.CONFLICT).json({message: 'Registration failed'});
        }

        // Store user data
        await userHelper.insertNewUser([username, email, passwordHash, is_admin]);


        const user = await userHelper.getUserByUsername(username);

        // Generate a JWT token
        const token = await generateToken(user.username, user.is_admin);

        res.status(statusCodes.CREATED).json(token);
    }
}

async function generateToken(username, is_admin) {
    return await jwt.sign({username: username, is_admin: is_admin}, secret, {
        expiresIn: '2h', // Token expiration time
    });
}

export async function editUser(req, res) {
    const username = req.params.username;
    const newData = req.body;
    if (!username || !newData) {
        res.status(statusCodes.BAD_REQUEST).json({message: 'Unable to update user due to the lack of data'});
    }
    const user = await userHelper.updateUser(username, newData.usernameForCreating, newData.is_adminForCreating);
    return res.status(statusCodes.CREATED).json({message: "User Edited"});
}

export async function getTestCasesByTesterName(req, res) {
    const username = req.params.username;
    const user = await userHelper.getUserByUsername(username);

    if (!user) {
        return res.status(statusCodes.NOT_FOUND).json({error: 'User not fond'});
    }
    const tests = await testHelper.getTestsByTesterName(username);
    return res.status(statusCodes.OK).json(tests);
}

export async function getTesters(req, res) {
    const testers = await userHelper.getAllTesters();
    res.status(statusCodes.OK).json(testers);
}

export async function checkChangePassToken(req, res) {
    const token = req.params.token;

    try {
        const decodedToken = await jsonwebtoken.verify(token, secretKey);
        const user = await userHelper.getUserByUsername(decodedToken.username);

        if (user.change_pass_token !== token) {
            return res.status(statusCodes.UNAUTHORIZED).json({error: "Invalid change password token"});
        }

        if (decodedToken.exp && Date.now() >= decodedToken.exp * 1000) {
            return res.status(statusCodes.UNAUTHORIZED).json({error: "Token has expired"});
        }

        return res.status(statusCodes.OK).json({message: "Token is valid"})

    } catch (err) {
        console.error("Error:", error.message);
        return res.status(statusCodes.UNAUTHORIZED).json({error: "Token is not valid"});
    }
}

export async function changePassword(req, res) {
    const token = req.headers.authorization;
    const newPassword = req.body.newPassword;
    const hashedPassword = await bcrypt.hash(newPassword, 10);

    let decodedToken;
    try {
        decodedToken = await jsonwebtoken.verify(token, secretKey);
    } catch (err) {
        console.error("Error:", error.message);
        return res.status(statusCodes.UNAUTHORIZED).json({error: "Token is not valid"});
    }
    const user = await userHelper.getUserByUsername(decodedToken.username);


    if (user.change_pass_token !== token) {
        return res.status(statusCodes.UNAUTHORIZED).json({error: "Invalid change password token"});
    }

    if (decodedToken.exp && Date.now() >= decodedToken.exp * 1000) {
        return res.status(statusCodes.UNAUTHORIZED).json({error: "Token has expired"});
    }

    try {
        await userHelper.updatePassword(hashedPassword, decodedToken.username);
    } catch (err) {
        console.error("Error:", error.message);
        return res.status(statusCodes.INTERNAL_SERVER_ERROR).json({error: "Change password failed"})
    }

    return res.status(statusCodes.OK).json({message: "Password Changed"})
}

export async function changePasswordEmail(req, res) {
    const body = req.body;
    const user = await userHelper.getUserByUsername(body.username);

    if (!user) {
        res.status(statusCodes.NOT_FOUND).json({message: 'User does not exist'});
    }
    if (user) {
        const resetToken = generateResetToken(user.username);
        const userEmail = user.email;

        await userHelper.insertChangePassToken(resetToken, user.username);

        sendResetEmail(userEmail, resetToken)
            .then(() => {
                res.status(200).json({message: 'Password reset initiated successfully.'});
            })
            .catch(error => {
                console.error("Error:", error.message);
                res.status(500).json({error: 'Error sending password reset email.'});
            });
    }
}

function generateResetToken(username) {

    const expirationTime = '1h';

    const payload = {
        type: 'password-reset',
        username: username
    };

    return jwt.sign(payload, secretKey, {expiresIn: expirationTime});
}

async function sendResetEmail(userEmail, resetToken) {
    const transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'clientonboardnoreply@gmail.com',
            pass: 'hjqx bocx vfim fixu',
        },
    });

    transporter.verify().then(console.log).catch(console.error);

    const mailOptions = {
        from: 'clientonboardnoreply@gmail.com',
        to: userEmail,
        subject: 'Password Reset',
        html: `
        <div style="font-family: 'Arial', sans-serif; padding: 20px; border: 1px solid #ddd; border-radius: 5px;">
            <h2 style="color: #333; text-align: center;">Password Reset</h2>
            <p style="font-size: 16px; color: #333; line-height: 1.5; text-align: center;">
                You have requested to reset your password. Click the link below to proceed:
            </p>
            <p style="text-align: center;">
                <a href="http://localhost:5173/reset-password/${resetToken}" style="display: inline-block; padding: 10px 20px; background-color: #007BFF; color: #fff; text-decoration: none; border-radius: 3px;">
                    Reset Password
                </a>
            </p>
            <p style="font-size: 14px; color: #777; text-align: center;">
                If you did not request this, please ignore this email.
            </p>
        </div>
    `,
    };

    return transporter.sendMail(mailOptions);
}