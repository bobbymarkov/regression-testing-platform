import * as jsonschema from 'jsonschema';
import statusCodes from "http-status-codes";

const validator = new jsonschema.Validator();

//Sample user schema

const schemas = {
    'user': {
        id: 'UserSchema',
        type: 'object',
        properties: {
            id: {type: 'string'},
            userEmail: {
                type: 'string',
                maxLength: 320,
                format: 'email'
            },
            password: {
                type: 'string',
                minLength: 8,
                pattern: '(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[#$@!%&*?_^])'
            }
        },
        required: ['userEmail', 'password']
    },
    'bugreport': {
        id: 'BugReportScheme',
        type: 'object',
        properties: {
            id: {type: 'number'},
            title: {type: 'string'},
            description: {type: 'string'},
            environment: {type: 'string'},
            product: {type: 'string'},
            blocking: {type: 'boolean'}
        },
        required: ['title', 'description', 'blocking']
    },
    'step': {
        id: 'StepScheme',
        type: 'object',
        properties: {
            description: {type: 'string'},
            status: {type: 'string'},
            testCaseId: {type: 'number'}
        },
        required: ['description', 'status', 'testCaseId']
    }

}


//Validator method, throw 400 if schema is not valid
export function validateOrThrow(obj, schema) {
    const result = validator.validate(obj, schemas[schema]);
    if (result.errors.length > 0) {
        const errorList = [];
        result.errors.forEach(error => {
            console.log('Error in validator with schema ' + schema, error);
            errorList.push((error.path[0] || '') + ' ' + error.message);
        });
        throw {statusCode: statusCodes.BAD_REQUEST, message: {errors: errorList}}
    }
}