export function durationInSeconds(totalDuration) {
    let result;
    const postgresDurationJsonString = JSON.stringify(totalDuration);
    const durationJsonObject = JSON.parse(postgresDurationJsonString);

    if (durationJsonObject) {
        let hoursInSeconds = 0;

        if (durationJsonObject.hours !== undefined){
            hoursInSeconds = durationJsonObject.hours ? 3600 * durationJsonObject.hours : 0;
        }

        const minutesInSeconds = durationJsonObject.minutes ? 60 * durationJsonObject.minutes : 0;
        const seconds = durationJsonObject.seconds ? durationJsonObject.seconds : 0;
        result = hoursInSeconds + minutesInSeconds + seconds;
        return result;
    }

    return null;
}

