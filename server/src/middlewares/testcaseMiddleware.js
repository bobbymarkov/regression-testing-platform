import statusCodes from "http-status-codes";

export function validateRequestBody(req, res, next) {
    const requiredFields = ['board_id', 'name', 'status', 'duration', 'due_date', 'weight', 'description', 'comment', 'environment'];

    for (const field of requiredFields) {
        if (!(field in req.body)) {
            return res.status(statusCodes.BAD_REQUEST).json({ error: `Missing required field: ${field}` });
        }
    }
    next();
}

export function testcaseExists(req, res, next) {
    const id = parseInt(req.params.id);

    return next();
}