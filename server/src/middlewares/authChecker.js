import statusCodes from "http-status-codes";
import jsonwebtoken from "jsonwebtoken";
import {secret} from "../controllers/accountController.js";

//TODO: Add roles to user

export async function loggedIn(req, res, next) {
    const token = req.header('Authorization');
    if (token === undefined) {
        return next(noAccess());
    }
    const tokenArray = token.split(" ");
    if (!(tokenArray.length === 2)) {
        return next(noAccess());
    }
    return next();
}

export async function isAdmin(req, res, next) {
    const decodedToken = await decodeToken(req, next);
    if (!(decodedToken.is_admin)) {
        return next(noAccess());
    }

    return next();
}

async function decodeToken(req, next) {
    const token = req.header('Authorization');
    const tokenArray = token.split(" ");

    try {
        return await jsonwebtoken.verify(tokenArray[1], secret);
    } catch (err) {
        return next(noAccess());
    }
}

function noAccess() {
    return {
        status: statusCodes.UNAUTHORIZED,
        message: "You are not authorized to do this action."
    }
}