import express from "express";
import upload from "../aws/s3_uploadfunc.js";
import * as attachmentController from "../controllers/attachmentController.js";

const router = express.Router();

router.post('/', upload.single('file'), attachmentController.postAnAttachment);

export default router;