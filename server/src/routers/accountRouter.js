import express from "express";
import * as AccountController from "../controllers/accountController.js";
import {isAdmin, loggedIn} from "../middlewares/authChecker.js"

const router = express.Router();

router.get('/', loggedIn, isAdmin, AccountController.getAllAccounts);
router.get('/testers', loggedIn, AccountController.getTesters);
router.get('/:username/testcases', loggedIn, AccountController.getTestCasesByTesterName);
router.get('/checkPassToken/:token', AccountController.checkChangePassToken);
router.put('/update-password', AccountController.changePassword)
router.post('/login', AccountController.logInUser);
router.post('/register', AccountController.registerNewUser);
router.post('/change-password', AccountController.changePasswordEmail)
router.put('/:username', loggedIn, isAdmin,  AccountController.editUser);

export default router;