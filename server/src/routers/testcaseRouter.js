import express from "express";
import * as testcaseController from '../controllers/testcaseController.js';
import * as bugReportController from '../controllers/bugReportController.js';
import {isAdmin, loggedIn} from "../middlewares/authChecker.js";


const router = express.Router();

router.get('/', loggedIn, testcaseController.getAllTestCases);
router.get('/products', loggedIn, testcaseController.getAllRelationsBetweenTestcasesAndProducts);
router.get('/:id', loggedIn, testcaseController.getTestCaseById);
router.get('/:id/steps', loggedIn, testcaseController.getStepsByTestCaseId);
router.get('/:id/products', loggedIn, testcaseController.getProductsByTestCaseId);
router.get('/:id/bugreports', loggedIn, bugReportController.getAllBugReportsByTestcaseID);

router.post('/', loggedIn, isAdmin, testcaseController.createTestCase);
router.post('/:id/products', loggedIn, isAdmin, testcaseController.addProductsUsingTestCaseId);

router.put('/:id', loggedIn, testcaseController.updateTestCaseUsingId);

router.delete('/:id', loggedIn, isAdmin, testcaseController.deleteTestCaseById);
router.delete('/:id/products/:product', loggedIn, isAdmin, testcaseController.deleteProductsUsingTestCaseId);

export default router;