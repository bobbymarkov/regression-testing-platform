import express from "express";
import * as BoardController from "../controllers/boardController.js";
import * as bugReportController from "../controllers/bugReportController.js";
import {loggedIn, isAdmin} from "../middlewares/authChecker.js";

const router = express.Router();

// GET CALL
router.get('/', loggedIn, BoardController.getAllBoards);
router.get('/:id', loggedIn, BoardController.getBoardById);
router.get('/:name', loggedIn, BoardController.getBoardByName);
router.get('/:id/testcases', loggedIn, BoardController.getAllTestcasesOfOneBoard);
router.get('/:id/progress', loggedIn, BoardController.getBoardProgress);
// POST CALL
router.put('/:id', loggedIn, isAdmin, BoardController.updateBoard);
// PUT CALL
router.post('/', loggedIn, isAdmin, BoardController.createBoard);
router.post('/duplicate', loggedIn, isAdmin, BoardController.duplicateBoard);
//DELETE CALL
router.delete('/:id', loggedIn, isAdmin, BoardController.deleteBoard);

//GET ALL BUG REPORTS VIA BOARD ID
router.get('/:id/bugreports', loggedIn, bugReportController.getAllBugReportsByBoardId);

export default router;