import express from 'express';
import * as controller from "../controllers/stepController.js";
import {isAdmin, loggedIn} from "../middlewares/authChecker.js";

const router = express.Router();

router.get("/", loggedIn, controller.getSteps);
router.get("/:id", loggedIn, controller.getStepById);
router.post("/", loggedIn, isAdmin, controller.addNewStep);
router.put("/:id", loggedIn, isAdmin, controller.editStepById);
router.delete("/:id", loggedIn, isAdmin, controller.deleteStepById);

export default router;