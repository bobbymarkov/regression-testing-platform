import express from "express";
import * as bugReportController from '../controllers/bugReportController.js'
import * as attachmentController from '../controllers/attachmentController.js'
import upload from "../aws/s3_uploadfunc.js";
import {loggedIn} from "../middlewares/authChecker.js";
const bugReportRouter = express.Router();

bugReportRouter.get('', loggedIn, bugReportController.getAllBugReports);
bugReportRouter.post('', loggedIn, bugReportController.postABugReport);
bugReportRouter.get('/:id', loggedIn, bugReportController.getABugReportByID);
bugReportRouter.patch('/:id', loggedIn, bugReportController.editABugReport);

// add attachment:
bugReportRouter.post('/:id/upload', loggedIn, upload.single('file'), attachmentController.postAnAttachment);
bugReportRouter.get('/:id/attachments', loggedIn, attachmentController.getAllAttachmentsByBugId);

export default bugReportRouter;
