import express from "express";
import testcaseRouter from "./routers/testcaseRouter.js";
import boardRouter from "./routers/boardRouter.js"
import bugReportRouter from "./routers/bugReportRouter.js";
import accountRouter from "./routers/accountRouter.js"
import stepRouter from "./routers/stepRouter.js"
import 'dotenv/config';

import cors from "cors";

const app = express();
const port = process.env.PORT;

app.use(express.json());
app.use(cors());

// routers
app.use('/boards', boardRouter);
app.use('/bugreports', bugReportRouter);
app.use('/accounts', accountRouter);
app.use('/testcases', testcaseRouter);
app.use('/steps', stepRouter);

// Global exception handler
app.use(function (err, req, res, next) {
    res.status(err.statusCode || 500).json({message: err.message || 'Something went wrong!'});
});

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
});

