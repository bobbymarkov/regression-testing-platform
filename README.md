
# PROJECT CLIENT ON BOARD

SCORION: Regression Test Management System


## Project goal
The goal of the project is to deliver fully operational web application of the regression test management system to Scorion.
To achieve this, the software must meet all the specified user stories and functional requirements.
Additionally, the project needs to achieve user-friendliness and aims to provide an intuitive interface that will enhance
user experience.

## Stakeholders
1. _Scorion (the client):_ Scorion is the client that requested the application to be developed by groups of students from Saxion.
The company has high interest, high influence over the project. The group has direct contact with Joran from Scorion for feedback on the development progress.
2. _System admin:_ System admin is responsible for managing and maintaining the regression testing system.
They have high interest, moderate influence on the system, their input is essential to operate a smooth system.
3. _Tester:_ They are responsible for testing and updating bug reports. They have high interest and moderate influence on the system as they will be the direct user aside from the admin.
4. _Customers of Scorion:_ Customers benefit indirectly from the application by receiving a more reliable and bug-free product. They have low interest and indirect influence on the system.
5. _Saxion students:_ Each student group represents the development team, responsible for designing and building the system. The group has high interest, low influence.

## Documentation overview
All the required documentation can be found in documentation.
In the direct folder are ClientLog recording the group's contact with Scorion, Code of Conduct (CoC), Plan of Approach (PoA), DailyScrum, Retrospectives, and Time registration.

APISpecifications, Functional design and System design can be found in documentation/technicalDocumentation.

<span style="color:red">Important note:</span>

To run the project, please refer to documentation/guides/dummyData.md to input dummy data into the application. Replace placeholder email with your actual email to receive password reset email.

Environmental variables are stored in individual .env file. Guide to .env files and the constants used in the project can be found in documentation/guides/dotenv.md 

## Members
The group is consisted of 8 people

1. Sasha Sukhinich

2. Are Durand

3. Minh Trang Tran

4. Kamila Nikitina

5. Yelizaveta Lazarevich

6. Borislav Markov

7. Hoang Cao Bach Dang

8. Koray Koca

