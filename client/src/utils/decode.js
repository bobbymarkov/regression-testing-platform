import {tokenStore} from "../store.js";

let token;

tokenStore.subscribe(value => {
    token = value.token;
});

export function isAdmin(){
    return decode().is_admin;
}

export function getUsername(){
    return decode().username;
}

function decode(){
    const base64String = token.split('.')[1];
    return JSON.parse(atob(base64String));
}