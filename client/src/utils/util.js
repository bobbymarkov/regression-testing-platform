import moment from "moment";

export function convertDate(date) {
    return moment(date, 'YYYY-MM-DD').format('MMM Do YYYY');
}

export function formatDuration({duration}) {
    let result;
    if (duration) {
        if (duration.hours) {
            result = duration.hours + " hours ";
        } else {
            result = "0 hours ";
        }

        if (duration.minutes) {
            result = result + duration.minutes + " minutes ";
        } else {
            result = result + "0 minutes ";
        }

        if (duration.seconds) {
            result = result + duration.seconds + " seconds ";
        } else {
            result = result + "0 seconds ";
        }
    } else {
        result = "0 hours 0 minutes 0 seconds";
    }

    return result;
}

export function addToPGInterval(interval, pgInterval) {
    let pgIntervalOnlySeconds = pgIntervalWithOnlySeconds(pgInterval);
    pgIntervalOnlySeconds.seconds += interval / 1000;
    return pgIntervalOnlySeconds;
}

export function pgIntervalWithOnlySeconds(pgInterval) {
    let seconds = 0;
    if (pgInterval !== null){
        if (!isNaN(pgInterval.seconds)) {
            seconds += pgInterval.seconds;
        }
        if (!isNaN(pgInterval.hours)) {
            seconds += pgInterval.hours * 60 * 60;
        }
        if (!isNaN(pgInterval.minutes)) {
            seconds += pgInterval.minutes * 60;
        }
    }

    return {
        seconds: seconds
    };
}

export function convertToPGInterval(interval) {
    return {
        seconds: interval / 1000
    };
}
