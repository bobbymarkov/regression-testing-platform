export function decodeToken(token) {
    if(!token) return null;
    const parts = token.split('.');
    if (parts.length !== 3) {
        throw new Error('Invalid JWT token');
    }

    const payload = parts[1];
    const decodedPayload = atob(payload);
    return JSON.parse(decodedPayload);
}

export function isAdmin(token){
    let decodedPayload = decodeToken(token)
    if(decodedPayload === null){
        return false
    }
    return decodedPayload.isAdmin
}