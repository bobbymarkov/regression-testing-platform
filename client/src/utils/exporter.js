import jsPDF from "jspdf";
import autoTable from "jspdf-autotable";
import {convertDate, formatDuration} from "./util.js";

export function exportToCSV(testcases, board, filteredProduct) {
    // Create CSV content
    const csvContent = [
        ['Test Id', 'Test name', 'Time', 'Due date', 'Weight', 'Product'],
        ...testcases
            .map(testcase => [
                testcase.id,
                testcase.name,
                formatDuration({ duration: testcase.duration }),
                convertDate(testcase.due_date),
                testcase.weight,
                testcase.product
            ])
    ]
        .map(row => row.join(','))
        .join('\n');

    const blob = new Blob([csvContent], { type: 'text/csv' });

    const link = document.createElement('a');
    link.href = window.URL.createObjectURL(blob);
    link.download = `${board.name}_${filteredProduct}_${new Date().toLocaleDateString()}.csv`;
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
}


export function exportToPDF(testcases, board, filteredProduct) {
    const doc = new jsPDF();
    autoTable(doc, {
        head: [['Test Id', 'Test name', 'Time', 'Due date', 'Weight', 'Product']],
        body: testcases
            .map(testcase => [
                testcase.id,
                testcase.name,
                formatDuration({duration: testcase.duration}),
                convertDate(testcase.due_date),
                testcase.weight,
                testcase.product
            ]),
    });
    let currentDate = new Date();
    const name = board.name + board.id + "_" + filteredProduct + "_" + currentDate.toLocaleDateString() + ".pdf";
    doc.save(name);
}
