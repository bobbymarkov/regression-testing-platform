import {tokenStore} from "../store.js";

let token;
tokenStore.subscribe(value => {
    token = value.token;
})

export async function fetchHelper(endpoint, method = 'GET', data = {}) {
    const apiUrl = `${import.meta.env.VITE_BASE_URL}${endpoint}`;

    const headers = {
    };

    if (token) {
        headers.Authorization = `Bearer ${token}`;
    }

    const options = {
        method: method,
        headers: headers
    }

    if (method !== 'GET') {
        options.body = JSON.stringify(data);
        headers['Content-Type'] = 'application/json';
    }

    const response = await fetch(apiUrl, options);

    if (response.ok) {
        if (method === 'DELETE' || method === 'PUT') {
            // We got an empty body, so we won't parse anything
            return true;
        }
        return await response.json();
    }

    const responseData = await response.json();
    throw {
        status: response.status,
        data: responseData
    };
}


export async function getAllUsers() {
    try {
        const response = await fetch(`${import.meta.env.VITE_BASE_URL}/accounts`, {
            headers: {
                "Authorization": `Bearer ${token}`
            }
        });
        return await response.json();
    } catch (error) {
        JSON.stringify(error);
    }
}

export async function getTestCase(id){
    return await fetchHelper(`/testcases/${id}`, 'GET');
}
