import { writable } from "svelte/store";

export const tokenStore = writable({token: ''});
export const usersList = writable([]);
export const accountToEdit = writable({username: '', password: '', is_admin: false});
export const boardAction = writable(false);