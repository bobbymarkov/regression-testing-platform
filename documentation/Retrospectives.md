# RETROSPECTIVE

## Retrospective Sprint 1
What went well
- Finished 90% of the initial declared user stories.
- Good communication within the group.
- Gave warnings, after which people become more responsible with being on time.
- Group members are respectful to one another.
- Client feedback indicated that the group has good progress.

What did not go well
- Merging had a lot of conflicts.
- Lack of experience in git.
- Time management, some group members are consistently late at the beginning of the project.

What needs to be improved
- Merge each part right after it works on the local branch.
- Every thursday of the week each group member should have something to show to the rest of the group.

## Retrospective Sprint 2
What went well
- People were more punctual.
- Main functionalities are finished
- Positive feedback from client after the sprint that the functionalities are good.

What did not go well
- Merge conflicts by the end of the sprint.

What needs to be improved
- Only allowed to do merge if someone checks the merge request.

## Retrospective Sprint 3
What went well
- General group performance is good.
- The presentation is good and well-prepared.
- Bug fixing and merging went smooth.
- Communication among group members are upheld, problems are promptly discussed.

What did not go well
- Different naming convention led to bugs after merging.
- The group created new issues for sprint, which affected the burn down chart in gitlab.

What needs to be improved
- Issue management on gitlab.
- Agreement on naming convention.