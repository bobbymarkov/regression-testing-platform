# CODE OF CONDUCT

The following Code of Conduct sets standards for how the group members will interact within the group, including
communication and approach to the project.

As an individual member of Group 1, we will conduct ourselves in a manner that centers mutual respect and responsibility
with the project. Within the duration of the project, the team members adhere to the following expected behaviors:

- Every member must treat one another with respect. We recognize that different viewpoints are normal and healthy in the
  decision-making process. We have the right to disagree, but we will express it in a manner that upholds mutual
  respect.
- Every member shall arrive on time and be present in class and meeting. In any case of absence, the member must inform
  all the other team members and the lecturer. We have established a meeting schedule to ensure regular communication
  and collaboration within our team. We have planned to hold at least one meeting each week, typically scheduled for
  Tuesday afternoon. If a member is still absent 15 minutes or more into a class or meeting, a warning strike will be issued.
- Effective communication is crucial for our team's success. We expect all team members to communicate clearly and
  promptly, whether it be through Discord, email, or in-person conversations. This includes informing the team if there
  are any personal or external issues that may impact their ability to complete their assigned tasks.
- When there is conflict between members, the team shall first try to resolve it among themselves. Should this approach
  proven unsuccessful, the issue will then be escalated to the lecturer.
- If a team member encounters challenges in finishing their assigned tasks, they must notify the team on time. The team
  will then discussion the solution or divide the task among the other members.

And we will attempt our best to stray from such examples of unacceptable behaviors. Specifically,

- Committing belittlement, harassment, and/or discrimination on fellow members.
- Disregarding the established meeting schedule without prior communication, especially without valid reasons, is
  unacceptable.
- Refusing to accept constructive criticism or resorting to personal attack when feedback is given.
- Failure to deliver assigned task on time without notifying other team members with a valid reason.
- Sharing the group work with other students who are not part of the group without prior agreement with other group
  members.

We forecast the consequences of mistreating the code of conduct can be:

- Strikes can be placed on the team member, regarding their misconduct.

By declaring the Code of Conducts, we believe that our team can accomplish our project goals while maintaining a
positive attitude during the duration of the project.

By placing this document in the project folder, all group members hereby agreed to adhere to the Code of Conducts.