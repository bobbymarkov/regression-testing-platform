# Plan of Approach

## Table Of Contents
1. [Roles](#roles)
2. [The Problem](#the-problem)
3. [Stakeholders](#stakeholders)
4. [Risks](#risks)
5. [Development Process](#development-process)
6. [Definition of Done](#definition-of-done)

## Roles

For the successful project it is important to clarify the responsibilities and expectations for each role. Our team made 
an agreement about roles, which will exist during the project development. Here is brief description of the positions:

### 1. Scrum Master
* **Role:** The Scrum Master is the coach of the team, who implement the Scrum practices and 
principles. 


* **Responsibilities:** Firstly, Scrum Master is the organiser of all the Scrum events, such as daily stand-ups, sprint planning, 
reviews and retrospectives. Secondly, helping to follow and the progress of the product with the introducing of the Scrum tools
  (like backlogs and boards). Besides, this role helps team to be more efficient and to get the right quality of the product, which the customer 
asking for.

  
* **Who is responsible:** During the meeting, it was agreed that every Sprint the Scrum Master will be changed. In this case,
more people can wear the role and develop its skills. For the coming project our team already determined who will be a Scrum Master for 
each Sprint: 
  * Sprint 1 - Trang Tran 
  * Sprint 2 - Hoang Cao Bach Dang
  * Sprint 3 - Borislav Markov
  
### 2. Developer 
* **Role:** In our team, each member is a full-stack developer. Basically, that is the person who is doing the actual work of
delivering the product. 


* **Responsibilities:** Creating and delivering a high-quality product, collaborate with teammates and building good communication,
  which will bring to the correct result, participate in the Scrum events, and constantly improve technical and soft skills.


* **Who is responsible:** To fulfil the requirement from the module, everyone from the project has to be full-stack developer.


### 3. Product Owner:
* **Role:** The Product Owner is the person who is requiring the product.


* **Responsibility:** Give the most clear picture of the required product and what is expecting from the final version of it.
  Mostly, the Product Owner just ensuring that the team understands items in the product backlog to the level needed, deciding
  on release dates and content, and representing the customer's interests through requirements and prioritization.


* **Who is responsible:** Anne Blom, the representative from the company Parantion.


### 4. Supervisor:
* **Role:** The supervisor is the person, who is coaching the team during the project.


* **Responsibility:** Supervisor helps in answering the detailed questions from the team members. Follow the process and 
* guide in the right direction. Reviewing the application, which will be developed by the group.


* **Who is responsible:** Joran, the representative from the company Parantion.

### 5. Contact person:
* **Role:** The person who contacts the company for arranging the meetings.


* **Responsibility:** Contact the product owner to ask about the meeting or cancel it. 


* **Who is responsible:** Sasha Sukhinich will be the person who will be responsible for contacting the company to 
make arrangement for the meetings.
***

## The Problem

Parantion, our client, is seeking a solution to enhance their manual regression testing system. They currently use the Jetbrains Space scrum board, and face challenges such as unrestricted editing and lack of specific features.

The proposed software should enable admins to configure sprints, manage test cases, and allow testers to conduct tests, report bugs, and update test case content.  Some missing but desired features include direct communication on blockers, comprehensive reporting, easy board reset for new sprints, progression tracking based on weight, and notifications for upcoming or overdue tests.

The client envisions future capabilities like measuring test walkthrough duration and predicting end times based on progress. The goal is to deliver a system that adds more value than their current Space setup for regression testing management.


***

## Stakeholders

**Parantion (Client Company)**
Parantion is a company based in Deventer, driven by a passion for people, development, innovation, and technology. Committed to delivering key innovative solutions, Parantion aims to make a meaningful impact by providing applications like Scorion Talent for talent development in education and companies, and Easion Survey for secure opinion gathering. Their mission is to encourage widespread adoption of their innovative apps, facilitating the mapping of innovation and development data to enhance organizational processes.

**Product Owner - Anne Blom**
Anne Blom serves as the product owner for the regression testing management system project. As a key stakeholder, Anne is responsible for defining and prioritizing requirements, ensuring the development aligns with Parantion's goals.

**Saxion**
Saxion, represented by Eelco Jannink, a programming teacher at Saxion, is the overseer of the project on the student side of things.

**Developers**
We, as the development team, are responsible for creating and implementing the new regression testing management system.

**Users**
The end users of the regression testing management system include various roles within Parantion, these could include:

* System Admins: Individuals responsible for overall configuration, managing sprints, and overseeing the testing environment.
* Testers: Individuals conducting manual regression tests, providing bug reports, and updating test case contents.
* Bugfixer: A role involved in addressing reported bugs and ensuring the system's functionality.
* Etc.: Other potential users with specific roles or responsibilities related to the system, contributing to its effective utilization within the organization.



***
## Risks
In addressing potential risks within our project plan, it is important to have clear protocols for issue resolution. 
If the group encounters any challenges, we must immediately attempt to resolve them, adhering strictly to our 
established code of conduct. However, if a problem is too complex for us to handle internally, we should not 
hesitate to consult with the teacher for guidance and assistance.

Another significant risk involves the interaction with a real client, a scenario unfamiliar to most of the 
team members. To deal with this, it is essential that we maintain respectful and effective communication with 
the client. We must prioritize understanding their needs and expectations in depth, as miscommunication or 
poor information gathering could lead to misunderstanding the client's vision, impacting the success 
of the final product.

***

## Development Process

During the project the team will work with the Scrum principals with using the Gitlab board. This methodology ensures efficiency and
adaptability throughout the workflow. 

### Sprint Overview and Milestones

Our project is structured into four sprints, each with specific start and end dates, goals, and deliverables:

1. Sprint 0 (Nov 20 - Dec 03): Initial setup and planning phase.
2. Sprint 1 (Dec 04 - Dec 17): Implement the basic requirements from the project owner, focus on the developing of the working system.
3. Sprint 2 (Dec 18 - Jan 14): Focus on development and iterative improvements.
4. Sprint 3 (Jan 15 - Jan 28): Finalization and preparation for the final deliverance.

**Deliverables**:
* 4th December: Deliverables include the plan of approach, code of conduct, technical and functional design (version 0.1), and individual reports.
* 29th January: The submission with the final version of the project, including all the documentation. 

### Repository and Issue Board Usage

* **Branching Strategy:** To maintain an efficient workflow and avoid merge conflicts, each team member will work on their own branch.
* **Task Management:** At the beginning of each sprint, and during it, team meetings will be held to discuss and assign 
tasks to each member. This ensures clarity and avoids misunderstandings about issues ownership.
* **Issue Tracking:** A separate board will be dedicated for bug tracking. Team members familiar with a specific issue can self-assign to resolve it.
For small issues, which can be resolved during the development, developers are allowed to solve them on the place immediately.

### Quality Assurance and Code Conventions

* **Code Conventions:** Our project must include only the clear names for the variables to maintain readability and consistency in our codebase.
If needed add comments to improve the understanding for another devloper. 
* **Quality Assurance Plan:** During the developing every team member will review processes to ensure that the quality of the
products is high and reliable of the software

### Change Log

| Date       | Version | Document Revision Description | Document Author |
|------------|---------|-------------------------------|-----------------|
| 03-12-2023 | 0.1     | Initial document              |                 |
|            |         |                               |                 |
|            |         |                               |                 |
|            |         |                               |                 |
|            |         |                               |                 |
|            |         |                               |                 |
|            |         |                               |                 |
|            |         |                               |                 |


This structured yet flexible approach to our development process is designed to maximize efficiency, minimize errors,
and ensure that all team members are clear on their roles and responsibilities. By adhering to these guidelines,
we aim to deliver a high-quality product that meets all our objectives within the set timeframe.
***
## Definition of Done

The Definition of Done is an important aspect of our project. It defines the criteria that must be met for each user story or task to be considered complete. Our Definition of Done includes the following criteria:

-  The code is tested, and all tests have been successful.
-  All the tests have been documented in the Test Plan documentation.
-  The code has been pushed to GitLab and other team members have been notified.
-  Questions and suggestions regarding the code have been addressed and incorporated into the code.
-  To accompany the code, the appropriate explanation must be written in the Technical Information, if the team has deemed this important. 

 