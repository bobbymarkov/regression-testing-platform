# System Design

## Table Of Contents
1. [Introduction](#introduction)
2. [General Overview](#general-overview)
3. [Design Considerations](#design-considerations)
4. [System Architecture](#system-architecture)
   1. [Logical View](#logical-view)
   2. [Hardware Architecture](#hardware-architecture)
   3. [Software Architecture](#software-architecture)
   4. [Information Architecture](#information-architecture)
   5. [Security Architecture](#security-architecture)
   6. [Performance](#performance)
5. [System Design](#system-design)
   1. [Database Design](#database-design)
   2. [User Interface Design](#user-interface-design)
   3. [Hardware Design](#hardware-design)
   4. [Software Design](#software-design)
   5. [Security Design](#security-design)
6. [Changelog](#changelog)

***

## Introduction

This document is the system design made for the regression test management system for Parantion. Parantion is an IT company from Deventer 
that designed and developed Scorion, which is an e-portfolio platform. For the development of this product and other products
Parantion used the Jetbrains Space scrum board to manage regression test. The system described in this document is meant 
to replace that and give Parantion a better tool to manage regression test and give insight in how long test take.

The document will first give a general overview of the system design. Then it will talk about the design considerations, 
which will be followed by the system architecture. In the system architecture the software architecture will be show and discussed. 
In that chapter the information and security architecture will also be discussed. The chapter will end with the performance.

The last chapter will be the actual system design in which there is a subchapter about the database design, user interface design,
hardware design, software design and security design.

***

## General Overview

The regression test management system is meant to give an easy way of managing regression test and give insight into 
the time needed for regression test and the amount of bugs that get encountered.

This system made as a web application. This means that the system will exist out of three separate parts: frontend, backend and database. The front end, 
this is the system the user uses. This frontend will do fetch calls to a backend server to get any data. The backend will be
the second system and is not much more than a REST API. The backend will do queries to the database.

***

## Design Considerations
### Goals and Guidelines
The primary design goal for the system is to provide an user-friendly interface. The system should provide a clear, visually appealing
user experience, ensuring that the users can easily navigate to different paths, create, manage, and monitor the board, testcases, and bug reports.

The system should also ensure data security and privacy via the use of authentication and authorization.
### Development Methods
The interface is designed with Figma.

The database server and usage is designed with ER diagram.
### Architectural Designs
The system is designed with a client-server architecture. The front end is developed using Vite and Svelte, 
and the backend is powered by Node.js to manage dependencies and Express.

The system follows the RESTful API design principles to manage communication between the frontend and the backend.
Using RESTful API ensures that the client-server interaction is optimized. This contributes directly to the scalability of the system.
Changes to the server side do not affect the client side and changes to the database layer can also be applied without writing the application logic.

The system also stores data in a relational database. Since the tables are created with constraints, data integrity is maintained. Using a database also
provides easy data management, especially in relation with API calls.

Attachments such as images, videos are stored in cloud service S3. AWS is a popular and reliable service provider with high scalability and high security.
Accesses are only provided with specific policy settings.
***

## System Architecture

### Logical View

#### The creation of the Boards, Testcases and Steps

On the Flowchart can be seen how the process of the creating the board and further is going.

Admin create or duplicate a board, create a new testcase inside the board with the following data
and new step inside the testcase. The actions are restricted for normal users. 

![Admin create new board, testcase, step](../FlowchartImages/AdminCreateBoardTestcase.png)

*Flowchart 1: Admin create new board, testcase, step*


#### The tester start testing by steps 

On the Flowchart can be seen which steps should the tester follow to succeed. 

Tester find a testcase, which must be done.
After set the timer and finish all the steps.
If there is a bug, the tester report about it.

The tester can be as a normal user, as an admin.


![Tester starts the testing process](../FlowchartImages/Tester%20doing%20tests.drawio.png)

*Flowchart 2: Tester starts the testing process*


#### The admin export the Board Report

On the Flowchart can be seen how the admin can export the board reports and which types.

After the logging, admin choose the Board to export. Choose the product which should be exported (Scorion/Easion/All) 
and type (.PDF/.CSV) and click to export.

![Admin export the board report](../FlowchartImages/ExportTheBoard.png)

*Flowchart 3: Admin export the board report*


#### The admin delete or edit the Board

On the Flowchart displayed, which actions are allowed to do with the board. The admin can delete the board and can edit
the board. If the admin wants to edit the board, he has to fill the new data in a small window.

![Admin make changes with the Board](../FlowchartImages/ActionsWithBoard.png)

*Flowchart 4: Admin makes changes with the Board*


#### The admin creates or edit the User

On the Flowchart is shown, how the admin is creating a new user or edit an existed one. 

If it is needed to create a new user, go to page User, choose to add a new user, fill the data and save it. 


If it is needed to edit a user, there are two ways, to change the username: choose the user to edit, fill a new username
and save it. To change the password: choose the user, click to reset the password and the user will get an email to 
reset the password. 


![Admin create or edit user](../FlowchartImages/EditUser.png)

*Flowchart 5: Admin creates or edit a User*

#### The user resets the password

On the Flowchart displayed, how the user is resetting the password. 

Firstly the user has to make a request to the admin to change the password.


The letter to reset the password is sent to the user's email. After opening the email, the user can find the link, which will follow
to the application to change password. Once a new password is submitted, it is set. 



![User resets the password](../FlowchartImages/ChangePassword.png)

*Flowchart 6: User resets the password*

### Hardware Architecture

```plantuml
[Users PC/Laptop] <-> [Backend Server]
[Backend Server] <-> [Database Server] 
```
As can be seen in the diagram above the system will exist out of three components that will communicate with each other.

### Software Architecture

```plantuml
[Frontend] <-> [REST API] : fetch
[REST API] <-> [SQL] : queries
```

The software that runs on the users PC/Laptop will be run in a browser and will be visual. This frontend software will 
send request for specific data to the backend server.
The software that runs on the backend server will be a RESTful API which will listen to the request from the frontend software.
The backend will then do a query to the database to and send the data it receives from the database to the frontend software.
The database will be a SQL relational database.

### Information Architecture
**User data**
- Information that are needed to create an user profile. 
This includes username, email, password, and user role in the system (is admin or not).

**Board data**
- Details about the board, including names, due dates.

**Test case**
- Information about the testcase: name, description, weight, due date, product, environment, the assigned tester, and status.
- Each test case can contain several steps.

**Step**
- Step contains name, status, and is linked to bug report in case a bug occurs during a step.

**Bug report**
- Information about the bug in relation to the step in which a bug happens.
- A bug report must contain name, description (steps to recreate the bug), and images or videos demonstrating the bug.

**Board report**
- Information about the current state of the board, including data about test case, time taken to finish a test, and weight.

### Security Architecture

The application will be designed with these certain security architecture requirements as below:
- Identity vetting: The system will require individual user account for each user.
   This includes authentication and authorization.
- Passwords are securely stored using bcrypt hashing. Since the application will not have access to actual user emails, account recovery mechanism will not be implemented. 
- The system uses encryption for sensitive information.
    HTTPS is used for securing data in transit. 
- Secure third-party integrations: The group will periodically review and update third-party libraries
    and dependencies to avoid vulnerabilities.

### Performance

The system will be used by approximately ten people, so the aim is to have the system work without lowering performance while ten people
are using it at the same time.  

All actions of the system should be within human patience, which means that something should not take longer then two seconds.

***

## System Design

### Database Design

The database will be a PostgresSQL database. 

```plantuml
skinparam linetype ortho 

entity testcase {
* id : serial <<PK>>
* board_id : integer <<FK>>
name_tester : varchar(50) <<FK>>
--
* name : varchar(70)
* status : varchar(30)
duration : interval
due_date : timestamp
weight : integer
description : text
environment : text 
}

entity testcase_product {
* testcase_id : int <<PK>><<FK>>
* product_name : varchar(50) <<PK>><<FK>>
--
}

entity product {
* name : varchar(50) <<PK>>
--
}

entity account {
* username : varchar(50) <<PK>>
--
* email : varchar(70) 
* password : text
* is_admin : boolean
change_pass_token : varchar
}

entity board {
* id : serial <<PK>>
--
* name : varchar(50) 
* due_date : timestamp
}

entity step {
* id : serial <<PK>>
* testcase_id : integer <<FK>>
--
* description : text 
status : varchar(30)
}

entity bug {
* id : serial <<PK>>
* step_id : integer <<FK>>
* name_tester : varchar(30) <<FK>>
--
* title : varchar(50)
description : text
environment : text
product : varchar(50)
blocking : boolean
status : varchar(50)
}

entity attachment {
* id : serial <<PK>>
--
* bug_id : int
* reference : text
}

account |o-right-o{ testcase 
account ||--o{ bug 
board ||-right-o{ testcase
testcase ||--o{ step
testcase ||-right-o{ testcase_product
product ||-up-o{ testcase_product
step ||-left-o{ bug
attachment ||-|{ bug
```

Above is the simple ER diagram displaying how the different entities in the database are related.

### User Interface Design

The UI will be designed with the idea to provide the users with an easy to navigate web application.
The UI will follow the Nielsen and Moloch's 10 guidelines.
- Users will be notified of changes in the system related to their account in a clear-visible manner such as web-push notification.
- Information will be presented in order, and can be sorted in order.
- User can control the information, including removing unwanted data, backtracking,and undoing previous actions.
- Error prevention will be presented to the user in modal pop-up to flag undesirable action, for example, blank input.
- Display will prioritize recognition over recalling. Only relevant data will be displayed on one instance of use at a time.
- Navigation bar will be implemented on the header. Filter will be implemented on sidebar.
- Design will be modern and minimalistic.
- Errors message will be displayed in clear and simple language.
- Documentation regarding usage of the system will be provided and written in markdown.

### Hardware Design

### Software Design

#### Frontend

The frontend software will be developed with the Svelte framework. Tailwind will also be used as a CSS framework. Both these 
frameworks will be used to create the pages and components with which the user interacts. The frontend software will also have
modules dedicated to verifying the input of the user and some dedicated to doing fetch request to the backend.

###### Pages' overview relationship

![](.SystemDesign_images/7322af3b.png)

###### Account page components' relationship

![](.SystemDesign_images/e6d17bdd.png)

###### ChangePassword page components' relationship

![](.SystemDesign_images/f613c7d6.png)

###### UserPage page components' relationship

![](.SystemDesign_images/ddcfdcb3.png)

###### AllBoards page components' relationship

![](.SystemDesign_images/34b3d1a9.png)

###### Board page components' relationship

![](.SystemDesign_images/4ac909d6.png)

###### Report page components' relationship

![](.SystemDesign_images/12de94db.png)

###### CreateTestCase page and TestCaseAdd page components' relationship

![](.SystemDesign_images/a91c262f.png)

###### BugReportAdd page and BugReportEdit page components' relationship

![](.SystemDesign_images/932421e0.png)

#### Classes

```plantuml
class board {
id : integer
name : string
due_date : Date
}

class account {
username : string
email : string
password : string
isAdmin : boolean
}

class testcase {
id : integer
name : string
status : string
duration : Interval
due_date : string
weight : integer
description : string
environment : string
}

class step {
id : integer
description : string
status : string
}

class bug {
id : integer
title : string
description : string
environment : string
blocking : boolean
status : string
}

class product {
name : string
}

class attachment {
id : integer
reference : string
}
 
board "1..1" <-- "0..*" testcase
account "1..1" <-- "0..*" testcase
step "0..*" -up-> "1..1" testcase
bug "0..*" --> "1..1" step
product "1..*" --> "0..*" testcase
attachment "0..*" -->  "1..1" bug 
```
Above you can see a class diagram of all the different resources used in the system. Most of these classes use the id property
as an identifier except account and product which use username and name instead.


#### REST API

The REST API software will be a made with the Express framework. This backend software exist out of an index.js file, a router module,
a controller module and a middleware module. Some middleware that is used in the REST API is validation of data send and authentication.

For explanation and details about the API specifications, please refer to documentation/technicalDocumentation/APISpecification.md
#### Frameworks

- **Svelte:** is used to help with create the frontend of the application. It is chosen over the other frameworks
  as the whole team has experience with using Svelte.
- **Express:** is used to help with creating endpoints in the backend of the application. It is chosen over the other framework
  as the whole team has experience with using Express.
- **Tailwind:** is used to help doing CSS easier. It is chosen over the other frameworks as some people already
  have experience with using Tailwind.

#### Libraries

***Frontend***

- **page:** is a small client side router library, that makes it easy to switch between pages without reloading the
  page too much.
- **validate.js:** is used to make it easier to validate if a JSON has the correct properties. This is a node.js
  library.
- **font-awesome:** is a node.js library that contains free icons. 
  This library provides a lot of flexibility in choosing and styling icons.
- **google-font:** is a font directory/API provided by Google with easy font import for font style.
- **jspdf:** is JavaScript library for generating PDF document in the browser.
- **dotenv:** is a library used for loading environment variables from a .env file, making it easier to manage configurations.

***Backend***

- **bcrypt:** is used to help with encrypting password that are created. It is a node.js library that contains functions,
  that help with encrypting.
- **cors:** is used to help with CORS. It is a node.js library that provides a middleware for the Express framework,
  that makes setting up the headers needed for CORS easy.
- **jsonwebtoken:**  is used to easily create JSON web tokens. This is a node.js library that provides methods 
  that makes creating and reading JSON web tokens easy.
- **jsonschema** used for validating JSON data against a specific schema.
- **validate.js:** is used to make it easier to validate if a JSON has the correct properties. This is a node.js
  library.
- **dotenv:** is a library used for loading environment variables from a .env file, making it easier to manage configurations.
- **aws-sdk** is a library provides JavaScript SDK for Amazon Web Server. This is used to store attachments.
- **multer** is middleware handling file uploads in Express.js application.
- **nodemailer** used to send emails from the server. This is used for password reset.



### Security Design

````text
           [User]
             &darr;
        [Security Frame]
             &darr;
         [Web server]
             &darr;
     [Application server]
             &darr;
  [Database]  [Replica database] [Cloud Storage]
````

The security design is illustrated in the simple diagram as above.
Access is controlled at the user level and application level via authentication, authorization, and encryption at security frame.
***

## Changelog

| Date       | Version | Document Revision Description                                | Document Author |
|------------|---------|--------------------------------------------------------------|-----------------|
| 03-12-2023 | 0.1     | Initial document                                             |                 |
| 07-12-2023 | 0.2     | Updated the database design                                  | Are Durand      |
| 19-12-2023 | 0.3     | Updated the database design, because some changes where made | Are Durand      |
| 17-01-2024 | 0.4     | Updated dependencies and libraries used in the project       | Trang Tran      |
| 21-01-2024 | 0.5     | Updated information architecture                             | Trang Tran      |
| 22-01-2024 | 0.6     | Updated design considerations                                | Trang Tran      |
| 27-01-2024 | 0.7     | Update the system architecture (logical view)                | Sasha Sukhinich |
| 28-01-2024 | 0.8     | Update the system front-end structure                        | Bach            |
