# API Specification
This document provides a comprehensive guide to the various API endpoints available, detailing the methods, parameters, and expected responses for each route. The API is organized into several routers, each serving a specific function within the application.
## Account Router
The Account Router deals with all operations related to user accounts. This includes retrieving user account details, managing user credentials, and handling tester accounts. It offers a range of GET, PUT, and POST requests to facilitate these functionalities.
### GET Requests
| GET             | `/api/accounts/`                |                                      |                                                                                                                                                                                                    |
|-----------------|---------------------------------|--------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|                 | Get a list of all user accounts |                                      |                                                                                                                                                                                                    |                            |                            |
| **Parameters:** | **Name**                        | **Type**                             | **Description**                                                                                                                                                                                    |
|                 | `token`                         | `auth`                               | Must be logged in as admin                                                                                                                                                                         |
| **Responses:**  | **Code**                        | **Description**                      | **Example JSON**                                                                                                                                                                                   |
|                 | `200`                           | Returns the list of user accounts    | [{"username": "&lt;username&gt;",<br>"email": "&lt;email&gt;",<br>"password": "&lt;password&gt;",<br>"is_admin": "&lt;is_admin&gt;",<br>"change_pass_token": "&lt;change_pass_token&gt;"},<br>...] |
|                 | `401`                           | No authorization token               | {"status": 401,<br>"message": "You are not authorized to do this action."}                                                                                                                         |     
|                 | `500`                           | Internal server error                |                                                                                                                                                                                                    |

| GET             | `/api/accounts/testers`           |                                     |                                                                                                                                                                                         |
|-----------------|-----------------------------------|-------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|                 | Get a list of all tester accounts |
| **Parameters:** | **Name**                          | **Type**                            | **Description**                                                                                                                                                                         |
|                 | `token`                           | `auth`                              | Must be logged in                                                                                                                                                                       |
| **Responses:**  | **Code**                          | **Description**                     | **Example JSON**                                                                                                                                                                        |
|                 | `200`                             | Returns the list of tester accounts | [{"username": "&lt;username&gt;",<br>"email": "&lt;email&gt;",<br>"password": "&lt;password&gt;",<br>"is_admin": "false",<br>"change_pass_token": "&lt;change_pass_token&gt;"},<br>...] |
|                 | `401`                             | No authorization token              | {"status": 401,<br>"message": "You are not authorized to do this action."}                                                                                                              |
|                 | `500`                             | Internal server error               |                                                                                                                                                                                         |

| GET             | `/api/accounts/{username}/testcases` |                                       |                                                                                                                                                                                                                                                                                                                                                   |
|-----------------|--------------------------------------|---------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|                 | Get test cases by tester username    |
| **Parameters:** | **Name**                             | **Type**                              | **Description**                                                                                                                                                                                                                                                                                                                                   |
|                 | `username`                           | `path`                                | Tester username to fetch test cases                                                                                                                                                                                                                                                                                                               |
|                 | `token`                              | `auth`                                | Must be logged in                                                                                                                                                                                                                                                                                                                                 |
| **Responses:**  | **Code**                             | **Description**                       | **Example JSON**                                                                                                                                                                                                                                                                                                                                  |
|                 | `200`                                | Returns the test cases for the tester | [{"id": &lt;id&gt;,<br>"board_id": &lt;board_id&gt;,<br>"name_tester": "&lt;name_tester&gt;",<br>"name": "&lt;name&gt;",<br>"status": "&lt;status&gt;",<br>"duration": &lt;duration&gt;,<br>"due_date": &lt;due_date&gt;,<br>"weight": &lt;weight&gt;,<br>"description": "&lt;description&gt;",<br>"environment": "&lt;environment&gt;"},<br>...] |
|                 | `401`                                | No authorization token                | {"status": 401,<br>"message": "You are not authorized to do this action."}                                                                                                                                                                                                                                                                        |
|                 | `404`                                | Username not found                    | {"error": "User not fond"}                                                                                                                                                                                                                                                                                                                        |
|                 | `500`                                | Internal server error                 |                                                                                                                                                                                                                                                                                                                                                   |

| GET             | `/api/accounts/checkPassToken/{token}`       |                            |                                                                                                                       |
|-----------------|----------------------------------------------|----------------------------|-----------------------------------------------------------------------------------------------------------------------|
|                 | Check the validity of a password reset token |
| **Parameters:** | **Name**                                     | **Type**                   | **Description**                                                                                                       |
|                 | `token`                                      | `path`                     | Token for password reset                                                                                              |
| **Responses:**  | **Code**                                     | **Description**            | **Example JSON**                                                                                                      |
|                 | `200`                                        | Token is valid             | {"message": "Token is valid"}                                                                                         |
|                 | `401`                                        | Token not found or expired | {"error": "Invalid change password token"} OR<br>{"error": "Token has expired"} OR<br>{"error": "Token is not valid"} |
|                 | `500`                                        | Internal server error      |                                                                                                                       |

### PUT Requests
| PUT             | `/api/accounts/update-password` |                               |                                                                                                                       |
|-----------------|---------------------------------|-------------------------------|-----------------------------------------------------------------------------------------------------------------------|
|                 | Update user password            |
| **Parameters:** | **Name**                        | **Type**                      | **Description**                                                                                                       |
|                 | `token`                         | `auth`                        | Must be logged in to update password                                                                                  |
|                 | `oldPassword`                   | `body`                        | Current user password                                                                                                 |
|                 | `newPassword`                   | `body`                        | New password to set                                                                                                   |
| **Responses:**  | **Code**                        | **Description**               | **Example JSON**                                                                                                      |
|                 | `200`                           | Password updated successfully | {"message": "Password Changed"}                                                                                       |
|                 | `401`                           | No authorization token        | {"error": "Token is not valid"} OR<br>{"error": "Invalid change password token"} OR<br>{"error": "Token has expired"} |
|                 | `500`                           | Internal server error         | {"error": "Change password failed"}                                                                                   |

### POST Requests
| POST            | `/api/accounts/login` |                             |                             |
|-----------------|-----------------------|-----------------------------|-----------------------------|
|                 | Log in user           |
| **Parameters:** | **Name**              | **Type**                    | **Description**             |
|                 | `username`            | `body`                      | User's username             |
|                 | `password`            | `body`                      | User's password             |
| **Responses:**  | **Code**              | **Description**             | **Example JSON**            |
|                 | `200`                 | User logged in successfully | {"token": "&lt;token&gt;"}  |
|                 | `400`                 | No username or password     | {"message": "Login failed"} |
|                 | `401`                 | Wrong username or password  | {message: "Login failed"}   |
|                 | `500`                 | Internal server error       |                             |

| POST            | `/api/accounts/register` |                               |                                    |
|-----------------|--------------------------|-------------------------------|------------------------------------|
|                 | Register new user        |
| **Parameters:** | **Name**                 | **Type**                      | **Description**                    |
|                 | `username`               | `body`                        | New user's username                |
|                 | `password`               | `body`                        | New user's password                |
|                 | `email`                  | `body`                        | New user's email                   |
| **Responses:**  | **Code**                 | **Description**               | **Example JSON**                   |
|                 | `201`                    | User registered successfully  | {"token": "&lt;token&gt;"}         |
|                 | `400`                    | No username or password       | {"message": "Registration failed"} |
|                 | `409`                    | Username already exists       | {"message": "Registration failed"} |
|                 | `500`                    | Internal server error         |                                    |

| POST            | `/api/accounts/change-password` |                                              |                                                       |
|-----------------|---------------------------------|----------------------------------------------|-------------------------------------------------------|
|                 | Request password change         |
| **Parameters:** | **Name**                        | **Type**                                     | **Description**                                       |
|                 | `username`                      | `body`                                       | User's username                                       |
| **Responses:**  | **Code**                        | **Description**                              | **Example JSON**                                      |
|                 | `200`                           | Password change email requested successfully | {"message": "Password reset initiated successfully."} |
|                 | `404`                           | Username not found                           | {"message": "User does not exist"}                    |
|                 | `500`                           | Internal server error                        | {"error": "Error sending password reset email."}      |

### PUT Requests
| PUT             | `/api/accounts/{username}` |                          |                                                                            |
|-----------------|----------------------------|--------------------------|----------------------------------------------------------------------------|
|                 | Edit user                  |
| **Parameters:** | **Name**                   | **Type**                 | **Description**                                                            |
|                 | `token`                    | `auth`                   | Must be logged in as admin to edit                                         |
|                 | `username`                 | `path`                   | User's username to edit                                                    |
|                 | `isAdmin`                  | `body`                   | Whether user is admin or not                                               |
| **Responses:**  | **Code**                   | **Description**          | **Example JSON**                                                           |
|                 | `200`                      | User edited successfully | {"message": "User Edited"}                                                 |
|                 | `400`                      | Username not found       | {"message": "Unable to update user due to the lack of data"}               |
|                 | `401`                      | No authorization token   | {"status": 401,<br>"message": "You are not authorized to do this action."} |
|                 | `500`                      | Internal server error    |                                                                            |

## Attachment Router
The Attachment Router is focused on uploading attachments.
### POST Requests
| POST            | `/api/attachments`      |                                  |                                        |
|-----------------|-------------------------|----------------------------------|----------------------------------------|
|                 | Upload a new attachment |
| **Parameters:** | **Name**                | **Type**                         | **Description**                        |
|                 | `file`                  | `file`                           | Attachment file to be uploaded         |
| **Responses:**  | **Code**                | **Description**                  | **Example JSON**                       |
|                 | `201`                   | Attachment uploaded successfully | {"msg": "File uploaded successfully!"} |
|                 | `400`                   | No file                          | {"error": "No file uploaded!"}         |
|                 | `500`                   | Internal server error            |                                        |

## Board Router
The Board Router is focused on the management of boards, a core aspect of our application. It includes endpoints for creating, updating, retrieving, and deleting boards, as well as handling test cases and bug reports associated with them.
### GET Requests
| GET             | `/api/boards`   |                              |                                                                                          |
|-----------------|-----------------|------------------------------|------------------------------------------------------------------------------------------|
|                 | Get all boards  |
| **Parameters:** | **Name**        | **Type**                     | **Description**                                                                          |
|                 | `token`         | `auth`                       | Must be logged in                                                                        |
| **Responses:**  | **Code**        | **Description**              | **Example JSON**                                                                         |
|                 | `200`           | Returns a list of all boards | [{"id": &lt;id&gt;,<br>"name": "&lt;name&gt;",<br>"due_date": &lt;due_date&gt;},<br>...] |
|                 | `401`           | No authentication token      | {"status": 401,<br>"message": "You are not authorized to do this action."}               |
|                 | `500`           | Internal server error        |                                                                                          |

| GET             | `/api/boards/{id}` |                                         |                                                                                |
|-----------------|--------------------|-----------------------------------------|--------------------------------------------------------------------------------|
|                 | Get a board by ID  |
| **Parameters:** | **Name**           | **Type**                                | **Description**                                                                |
|                 | `id`               | `path`                                  | Board ID                                                                       |
|                 | `token`            | `auth`                                  | Must be logged in                                                              |
| **Responses:**  | **Code**           | **Description**                         | **Example JSON**                                                               |
|                 | `200`              | Returns the board with the specified ID | {"id": &lt;id&gt;,<br>"name": "&lt;name&gt;",<br>"due_date": &lt;due_date&gt;} |
|                 | `400`              | No id                                   | {"error": "Board Id is missing"}                                               |
|                 | `401`              | No authentication token                 | {"status": 401,<br>"message": "You are not authorized to do this action."}     |
|                 | `404`              | Board not found                         | {"error": "Board not found"}                                                   |
|                 | `500`              | Internal server error                   |                                                                                |

| GET             | `/api/boards/{name}` |                                           |                                                                                |
|-----------------|----------------------|-------------------------------------------|--------------------------------------------------------------------------------|
|                 | Get a board by name  |
| **Parameters:** | **Name**             | **Type**                                  | **Description**                                                                |
|                 | `name`               | `path`                                    | Board name                                                                     |
|                 | `token`              | `auth`                                    | Must be logged in                                                              | 
| **Responses:**  | **Code**             | **Description**                           | **Example JSON**                                                               |
|                 | `200`                | Returns the board with the specified name | {"id": &lt;id&gt;,<br>"name": "&lt;name&gt;",<br>"due_date": &lt;due_date&gt;} |
|                 | `400`                | No name of board                          | {"error": "Board name is missing"}                                             |
|                 | `401`                | No authentication token                   | {"status": 401,<br>"message": "You are not authorized to do this action."}     |
|                 | `404`                | Board not found                           | {"error": "Board not found"}                                                   |
|                 | `500`                | Internal server error                     |                                                                                |

| GET             | `/api/boards/{id}/testcases`  |                                     |                                                                                                                                                                                                                                                                                                                                                   |
|-----------------|-------------------------------|-------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|                 | Get all test cases of a board |
| **Parameters:** | **Name**                      | **Type**                            | **Description**                                                                                                                                                                                                                                                                                                                                   |
|                 | `id`                          | `path`                              | Board ID                                                                                                                                                                                                                                                                                                                                          |
|                 | `token`                       | `auth`                              | Must be logged in                                                                                                                                                                                                                                                                                                                                 |
| **Responses:**  | **Code**                      | **Description**                     | **Example JSON**                                                                                                                                                                                                                                                                                                                                  |
|                 | `200`                         | Returns all test cases of the board | [{"id": &lt;id&gt;,<br>"board_id": &lt;board_id&gt;,<br>"name_tester": "&lt;name_tester&gt;",<br>"name": "&lt;name&gt;",<br>"status": "&lt;status&gt;",<br>"duration": &lt;duration&gt;,<br>"due_date": &lt;due_date&gt;,<br>"weight": &lt;weight&gt;,<br>"description": "&lt;description&gt;",<br>"environment": "&lt;environment&gt;"},<br>...] |
|                 | `401`                         | No authentication token             | {"status": 401,<br>"message": "You are not authorized to do this action."}                                                                                                                                                                                                                                                                        |
|                 | `500`                         | Internal server error               |                                                                                                                                                                                                                                                                                                                                                   |

| GET             | `/api/boards/{id}/progress` |                                   |                                                                                      |
|-----------------|-----------------------------|-----------------------------------|--------------------------------------------------------------------------------------|
|                 | Get the progress of a board |
| **Parameters:** | **Name**                    | **Type**                          | **Description**                                                                      |
|                 | `id`                        | `path`                            | Board ID                                                                             |
|                 | `token`                     | `auth`                            | Must be logged in                                                                    |
| **Responses:**  | **Code**                    | **Description**                   | **Example JSON**                                                                     |
|                 | `200`                       | Returns the progress of the board | {"weight": &lt;weight&gt;,<br>"bugs": &lt;bugs&gt;,<br>"duration": &lt;duration&gt;} |
|                 | `401`                       | No authentication token           | {"status": 401,<br>"message": "You are not authorized to do this action."}           |
|                 | `500`                       | Internal server error             |                                                                                      |

| GET             | `/api/boards/{id}/bugreports`   |                                             |                                                                                                                                                                                                                                                                                                                       |
|-----------------|---------------------------------|---------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|                 | Get all bug reports for a board |
| **Parameters:** | **Name**                        | **Type**                                    | **Description**                                                                                                                                                                                                                                                                                                       |
|                 | `id`                            | `path`                                      | Board ID                                                                                                                                                                                                                                                                                                              |
|                 | `token`                         | `auth`                                      | Must be logged in                                                                                                                                                                                                                                                                                                     |
| **Responses:**  | **Code**                        | **Description**                             | **Example JSON**                                                                                                                                                                                                                                                                                                      |
|                 | `200`                           | Returns a list of bug reports for the board | [{"id": &lt;id&gt;,<br>"step_id": &lt;step_id&gt;,<br>"name_tester": "&lt;name_tester&gt;",<br>title": "&lt;title&gt;",<br>"description": "&lt;description&gt;",<br>"environment": "&lt;environment&gt;",<br>"product": "&lt;product&gt;",<br>"blocking": "&lt;blocking&gt;",<br>"status": "&lt;status&gt;"},<br>...] |
|                 | `401`                           | No authentication token                     | {"status": 401,<br>"message": "You are not authorized to do this action."}                                                                                                                                                                                                                                            |
|                 | `500`                           | Internal server error                       |                                                                                                                                                                                                                                                                                                                       |

### PUT Requests
| PUT             | `/api/boards/{id}`  |                            |                                                                            |
|-----------------|---------------------|----------------------------|----------------------------------------------------------------------------|
|                 | Update a board      |
| **Parameters:** | **Name**            | **Type**                   | **Description**                                                            |
|                 | `id`                | `path`                     | Board ID                                                                   |
|                 | `token`             | `auth`                     | Must be logged in as admin to update a board                               |
|                 | `name`              | `body`                     | New name for the board                                                     |
|                 | `due_date`          | `body`                     | New due day for the board                                                  |
| **Responses:**  | **Code**            | **Description**            | **Example JSON**                                                           |
|                 | `200`               | Board updated successfully | {"message": "Updated the board"}                                           |
|                 | `400`               | No id of board             | {"error": "Board Id is missing"}                                           |
|                 | `401`               | No authentication token    | {"status": 401,<br>"message": "You are not authorized to do this action."} |
|                 | `404`               | Board not found            | {"error": "Board not found"}                                               |
|                 | `500`               | Internal server error      | {"error": "Failed to update the board"}                                    |

### POST Requests
| POST            | `/api/boards`      |                              |                                                                                          |
|-----------------|--------------------|------------------------------|------------------------------------------------------------------------------------------|
|                 | Create a new board |
| **Parameters:** | **Name**           | **Type**                     | **Description**                                                                          |
|                 | `token`            | `auth`                       | Must be logged in as admin to create a new board                                         |
|                 | `name`             | `body`                       | New name for the board                                                                   |
|                 | `due_date`         | `body`                       | New due day for the board                                                                |
| **Responses:**  | **Code**           | **Description**              | **Example JSON**                                                                         |
|                 | `201`              | Board created successfully   | [{"id": &lt;id&gt;,<br>"name": "&lt;name&gt;",<br>"due_date": &lt;due_date&gt;},<br>...] |
|                 | `400`              | No name or due_date of board | {"error": "Name is required"} OR {"error": "Date is required"}                           |
|                 | `401`              | No authentication token      | {"status": 401,<br>"message": "You are not authorized to do this action."}               |
|                 | `500`              | Internal server error        |                                                                                          |

| POST            | `/api/boards/duplicate`     |                               |                                                                                |
|-----------------|-----------------------------|-------------------------------|--------------------------------------------------------------------------------|
|                 | Duplicate an existing board |
| **Parameters:** | **Name**                    | **Type**                      | **Description**                                                                |
|                 | `token`                     | `auth`                        | Must be logged in as admin to duplicate a board                                |
|                 | `name`                      | `body`                        | New name for the board                                                         |
|                 | `due_date`                  | `body`                        | New due day for the board                                                      |
| **Responses:**  | **Code**                    | **Description**               | **Example JSON**                                                               |
|                 | `201`                       | Board duplicated successfully | {"id": &lt;id&gt;,<br>"name": "&lt;name&gt;",<br>"due_date": &lt;due_date&gt;} |
|                 | `400`                       | No name or due_date of board  | {"error": "Name is required"} OR {"error": "Date is required"}                 |
|                 | `401`                       | No authentication token       | {"status": 401,<br>"message": "You are not authorized to do this action."}     |
|                 | `500`                       | Internal server error         |                                                                                |

### DELETE Requests
| DELETE          | `/api/boards/:id` |                            |                                                                            |
|-----------------|-------------------|----------------------------|----------------------------------------------------------------------------|
|                 | Delete a board    |
| **Parameters:** | **Name**          | **Type**                   | **Description**                                                            |
|                 | `id`              | `path`                     | Board ID                                                                   |
|                 | `token`           | `auth`                     | Must be logged in as admin to delete a board                               |
| **Responses:**  | **Code**          | **Description**            | **Example JSON**                                                           |
|                 | `204`             | Board deleted successfully | {"message": "Board deleted successfully"}                                  |
|                 | `400`             | No id of board             | {"error": "Board ID is missing"}                                           |
|                 | `401`             | No authentication token    | {"status": 401,<br>"message": "You are not authorized to do this action."} |
|                 | `404`             | Board not found            | {"error": "Board not found"}                                               |
|                 | `500`             | Internal server error      |                                                                            |


## Bug Report Router
The Bug Report Router is dedicated to managing bug reports. It supports various operations such as creating, retrieving, editing, and uploading files related to bug reports.
### GET Requests
| GET             | `/api/bugreports`            |                                 |                                                                                                                                                                                                                                                                                                                       |
|-----------------|------------------------------|---------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|                 | Get a list of all bugreports |
| **Parameters:** | **Name**                     | **Type**                        | **Description**                                                                                                                                                                                                                                                                                                       |
|                 | `token`                      | `auth`                          | Must be logged in to get all bug reports                                                                                                                                                                                                                                                                              |
| **Responses:**  | **Code**                     | **Description**                 | **Example JSON**                                                                                                                                                                                                                                                                                                      |
|                 | `200`                        | Returns everything              | [{"id": &lt;id&gt;,<br>"step_id": &lt;step_id&gt;,<br>"name_tester": "&lt;name_tester&gt;",<br>title": "&lt;title&gt;",<br>"description": "&lt;description&gt;",<br>"environment": "&lt;environment&gt;",<br>"product": "&lt;product&gt;",<br>"blocking": "&lt;blocking&gt;",<br>"status": "&lt;status&gt;"},<br>...] |
|                 | `401`                        | There is no authorization token | {"status": 401,<br>"message": "You are not authorized to do this action."}                                                                                                                                                                                                                                            |
|                 | `500`                        | Internal server error           |                                                                                                                                                                                                                                                                                                                       |

| GET             | `/api/bugreports/{id}`     |                                      |                                                                                                                                                                                                                                                                                                             |
|-----------------|----------------------------|--------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|                 | Get a bug report by its id |
| **Parameters:** | **Name**                   | **Type**                             | **Description**                                                                                                                                                                                                                                                                                             |
|                 | `id`                       | `path`                               | Bug report id to find                                                                                                                                                                                                                                                                                       |
|                 | `token`                    | `auth`                               | Must be logged in to get a bug report                                                                                                                                                                                                                                                                       |
| **Responses:**  | **Code**                   | **Description**                      | **Example JSON**                                                                                                                                                                                                                                                                                            |
|                 | `200`                      | Returns everything                   | {"id": &lt;id&gt;,<br>"step_id": &lt;step_id&gt;,<br>"name_tester": "&lt;name_tester&gt;",<br>title": "&lt;title&gt;",<br>"description": "&lt;description&gt;",<br>"environment": "&lt;environment&gt;",<br>"product": "&lt;product&gt;",<br>"blocking": "&lt;blocking&gt;",<br>"status": "&lt;status&gt;"} |
|                 | `401`                      | There is no authorization token      | {"status": 401,<br>"message": "You are not authorized to do this action."}                                                                                                                                                                                                                                  |
|                 | `404`                      | Bug report with such id is not found | {"error": "Bug report with ID not found"}                                                                                                                                                                                                                                                                   |
|                 | `500`                      | Internal server error                |                                                                                                                                                                                                                                                                                                             |

| GET             | `/api/bugreports/{id}/attachments`   |                                 |                                                                                                |
|-----------------|--------------------------------------|---------------------------------|------------------------------------------------------------------------------------------------|
|                 | Get all attachments for a bug report |
| **Parameters:** | **Name**                             | **Type**                        | **Description**                                                                                |
|                 | `id`                                 | `path`                          | Bug report id to find                                                                          |
|                 | `token`                              | `auth`                          | Must be logged in to get attachments                                                           |
| **Responses:**  | **Code**                             | **Description**                 | **Example JSON**                                                                               |
|                 | `200`                                | Returns everything              | [ {"id": &lt;id&gt;,<br>"bug_id": &lt;bug_id&gt;,<br>"reference": "&lt;reference&gt;"}<br>...] |
|                 | `401`                                | There is no authorization token | {"status": 401,<br>"message": "You are not authorized to do this action."}                     |
|                 | `500`                                | Internal server error           |                                                                                                |

### POST Requests
| POST            | `/api/bugreports`       |                                               |                                                                            |
|-----------------|-------------------------|-----------------------------------------------|----------------------------------------------------------------------------|
|                 | Create a new bug report |
| **Parameters:** | **Name**                | **Type**                                      | **Description**                                                            |
|                 | `token`                 | `auth`                                        | Must be logged in to create a bug report                                   |
|                 | `step_id`               | `body`                                        | Id of the step this bug report is connected to                             |
|                 | `name_tester`           | `body`                                        | Name of the tester, who registers this bug                                 |
|                 | `title`                 | `body`                                        | Name of the bug                                                            |
|                 | `description`           | `body`                                        | Description of the bug                                                     |
|                 | `environment`           | `body`                                        | Environment of the testcase, this bug is connected to                      |
|                 | `blocking`              | `body`                                        | Is this a blocking bug or not                                              |
|                 | `product`               | `body`                                        | Product of the testcase, this bug is connected to                          |
|                 | `status`                | `body`                                        | Status of the bug                                                          |
| **Responses:**  | **Code**                | **Description**                               | **Example JSON**                                                           |
|                 | `201`                   | Successfully placed a bug                     | {"id": &lt;id&gt;}                                                         |
|                 | `400`                   | Some body parameters are missing or incorrect | {"statusCode": 400, "message": {errors: "&lt;error_list&gt;"}}             |
|                 | `401`                   | There is no authorization token               | {"status": 401,<br>"message": "You are not authorized to do this action."} |
|                 | `500`                   | Internal server error                         |                                                                            |

| POST            | `/api/bugreports/{id}/upload`      |                                               |                                                                            |
|-----------------|------------------------------------|-----------------------------------------------|----------------------------------------------------------------------------|
|                 | Upload a file related to bugreport |
| **Parameters:** | **Name**                           | **Type**                                      | **Description**                                                            |
|                 | `token`                            | `auth`                                        | Must be logged in to upload attachment                                     |
|                 | `id`                               | `path`                                        | Id of the  bugreport this attachment is related to                         |
|                 | `file`                             | `body`                                        | The uploaded file information, including its S3 URL                        |
| **Responses:**  | **Code**                           | **Description**                               | **Example JSON**                                                           |
|                 | `201`                              | Successfully uploaded attachment              | {"msg": "File uploaded successfully!"}                                     |
|                 | `400`                              | Some body parameters are missing or incorrect | {"error": "No file uploaded!"}                                             |
|                 | `401`                              | There is no authorization token               | {"status": 401,<br>"message": "You are not authorized to do this action."} |
|                 | `500`                              | Internal server error                         |                                                                            |

### PATCH Requests
| PATCH           | `/api/bugreports/{id}`      |                                      |                                                                            |
|-----------------|-----------------------------|--------------------------------------|----------------------------------------------------------------------------|
|                 | Edit a bug report by its id |
| **Parameters:** | **Name**                    | **Type**                             | **Description**                                                            |
|                 | `id`                        | `path`                               | Bug report id to find                                                      |
|                 | `token`                     | `auth`                               | Must be logged in to edit a bug report                                     |
| **Responses:**  | **Code**                    | **Description**                      | **Example JSON**                                                           |
|                 | `200`                       | Successfully updated bug report      | {}                                                                         |
|                 | `401`                       | There is no authorization token      | {"status": 401,<br>"message": "You are not authorized to do this action."} |
|                 | `400`                       | Internal server error                | {"statusCode": 400, "message": {errors: "&lt;error_list&gt;"}}             |
|                 | `404`                       | Bug report with such id is not found | {"error": "Bug report with ID not found!"}                                 |
|                 | `500`                       | Internal server error                |                                                                            |

## Step Router
The Step Router manages steps, which are individual components or tasks within test cases.
### GET Requests
| GET             | `/api/steps/`           |                                 |                                                                                                                                                                                                    |
|-----------------|-------------------------|---------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|                 | Get a list of all steps |                                 |                                                                                                                                                                                                    |
| **Parameters:** | **Name**                | **Type**                        | **Description**                                                                                                                                                                                    |
|                 | `token`                 | `auth`                          | Must be logged in to get all steps                                                                                                                                                                 |
| **Responses:**  | **Code**                | **Description**                 | **Example JSON**                                                                                                                                                                                   |
|                 | `200`                   | Returns everything              | {"msg": "Retrieved all steps successfully!",<br>"steps": [{"id": &lt;id&gt;,<br>"testcase_id": &lt;testcase_id&gt;,<br>"description": "&lt;description&gt;",<br>"status": "&lt;status&gt;"}, ...]} |
|                 | `401`                   | There is no authorization token | {"status": 401,<br>"message": "You are not authorized to do this action."}                                                                                                                         |
|                 | `404`                   | No steps found                  | {"error": "Retrieving all steps failed!"}                                                                                                                                                          |
|                 | `500`                   | Internal server error           |                                                                                                                                                                                                    |

| GET             | `/api/steps/{id}` |                                 |                                                                                                                                                                                               |
|-----------------|-------------------|---------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|                 | Get a step by id  |                                 |                                                                                                                                                                                               |
| **Parameters:** | **Name**          | **Type**                        | **Description**                                                                                                                                                                               |
|                 | `token`           | `auth`                          | Must be logged in to get a step                                                                                                                                                               |
| **Responses:**  | **Code**          | **Description**                 | **Example JSON**                                                                                                                                                                              |
|                 | `200`             | Returns everything              | {"msg": "Retrieved step with id successfully!",<br>"step": {"id": &lt;id&gt;,<br>"testcase_id": &lt;testcase_id&gt;,<br>"description": "&lt;description&gt;",<br>"status": "&lt;status&gt;"}} |
|                 | `400`             | There is no id in parameters    | {"msg": "Missing Id when fetching getStepById!"}                                                                                                                                              |
|                 | `401`             | There is no authorization token | {"status": 401,<br>"message": "You are not authorized to do this action."}                                                                                                                    |
|                 | `404`             | No steps found                  | {"error": "Retrieved step with id failed!"}                                                                                                                                                   |
|                 | `500`             | Internal server error           |                                                                                                                                                                                               |

### POST Requests
| POST            | `/api/steps/` |                                 |                                                                                                                                                                                |
|-----------------|---------------|---------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|                 | Create a step |
| **Parameters:** | **Name**      | **Type**                        | **Description**                                                                                                                                                                |
|                 | `token`       | `auth`                          | Must be logged in to add a step                                                                                                                                                |
|                 | `testCaseId`  | `body`                          | Id of the testcase this step belongs to                                                                                                                                        |
| **Responses:**  | **Code**      | **Description**                 | **Example JSON**                                                                                                                                                               |
|                 | `200`         | Successfully created a step     | {"msg": "Add step successfully",<br>"step": {"id": &lt;id&gt;,<br>"testcase_id": &lt;testcase_id&gt;,<br>"description": "&lt;description&gt;",<br>"status": "&lt;status&gt;"}} |
|                 | `400`         | There is no step in the body    | {"msg": "Missing step information!"}                                                                                                                                           |
|                 | `401`         | There is no authorization token | {"status": 401,<br>"message": "You are not authorized to do this action."}                                                                                                     |
|                 | `404`         | Adding new step failed          | {"error": "Adding new step failed!"}                                                                                                                                           |
|                 | `500`         | Internal server error           |                                                                                                                                                                                |

### PUT Requests
| PUT             | `/api/steps/{id}`    |                                  |                                                                                                                                                                                 |
|-----------------|----------------------|----------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|                 | Edit a specific step |
| **Parameters:** | **Name**             | **Type**                         | **Description**                                                                                                                                                                 |
|                 | `token`              | `auth`                           | Must be logged in tto edit a step                                                                                                                                               |
|                 | `id`                 | `path`                           | Step id to edit                                                                                                                                                                 |
|                 | `description`        | `body`                           | Description of the step                                                                                                                                                         |
|                 | `status`             | `body`                           | Status of the step                                                                                                                                                              |
|                 | `testCaseId`         | `body`                           | Id of the testcase the step belongs to                                                                                                                                          |
| **Responses:**  | **Code**             | **Description**                  | **Example JSON**                                                                                                                                                                |
|                 | `200`                | Successfully updated a step      | {"msg": "Edit step successfully",<br>"step": {"id": &lt;id&gt;,<br>"testcase_id": &lt;testcase_id&gt;,<br>"description": "&lt;description&gt;",<br>"status": "&lt;status&gt;"}} |
|                 | `400`                | There is no valid id in the path | {"msg": "Missing step ID when EDITING step"}                                                                                                                                    |
|                 | `401`                | There is no authorization token  | {"status": 401,<br>"message": "You are not authorized to do this action."}                                                                                                      |
|                 | `404`                | Updating a step failed           | {"error": "Edit step failed!"}                                                                                                                                                  |
|                 | `500`                | Internal server error            |                                                                                                                                                                                 |

### DELETE Requests
| DELETE          | `/api/steps/{id}`      |                                  |                                                                            |
|-----------------|------------------------|----------------------------------|----------------------------------------------------------------------------|
|                 | Delete a specific step |                                  |                                                                            |
| **Parameters:** | **Name**               | **Type**                         | **Description**                                                            |
|                 | `token`                | `auth`                           | Must be logged in to delete a step                                         |
|                 | `id`                   | `path`                           | Step id to delete                                                          |
| **Responses:**  | **Code**               | **Description**                  | **Example JSON**                                                           |
|                 | `200`                  | Successfully deleted a step      | {"msg": "Delete successfully!"}                                            |
|                 | `400`                  | There is no valid id in the path | {"msg": "Missing step ID when DELETING!"}                                  |
|                 | `401`                  | There is no authorization token  | {"status": 401,<br>"message": "You are not authorized to do this action."} |
|                 | `500`                  | Internal server error            |                                                                            |

## Testcase Router
The Testcase Router handles all functionalities related to test cases, from creation to deletion, including managing their association with products.
### GET Requests
| GET             | `/api/testcases/`           |                                 |                                                                                                                                                                                                                                                                                                                                                   |
|-----------------|-----------------------------|---------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|                 | Get a list of all testcases |                                 |                                                                                                                                                                                                                                                                                                                                                   |
| **Parameters:** | **Name**                    | **Type**                        | **Description**                                                                                                                                                                                                                                                                                                                                   |
|                 | `token`                     | `auth`                          | Must be logged in to get all testcases                                                                                                                                                                                                                                                                                                            |
| **Responses:**  | **Code**                    | **Description**                 | **Example JSON**                                                                                                                                                                                                                                                                                                                                  |
|                 | `200`                       | Returns everything              | [{"id": &lt;id&gt;,<br>"board_id": &lt;board_id&gt;,<br>"name_tester": "&lt;name_tester&gt;",<br>"name": "&lt;name&gt;",<br>"status": "&lt;status&gt;",<br>"duration": &lt;duration&gt;,<br>"due_date": &lt;due_date&gt;,<br>"weight": &lt;weight&gt;,<br>"description": "&lt;description&gt;",<br>"environment": "&lt;environment&gt;"},<br>...] |
|                 | `401`                       | There is no authorization token | {"status": 401,<br>"message": "You are not authorized to do this action."}                                                                                                                                                                                                                                                                        |
|                 | `500`                       | Internal server error           |                                                                                                                                                                                                                                                                                                                                                   |

| GET             | `/api/testcases/products`                                  |                                 |                                                                                           |
|-----------------|------------------------------------------------------------|---------------------------------|-------------------------------------------------------------------------------------------|
|                 | Get a list of all relations between testcases and products |
| **Parameters:** | **Name**                                                   | **Type**                        | **Description**                                                                           |
|                 | `token`                                                    | `auth`                          | Must be logged in to get a list                                                           |
| **Responses:**  | **Code**                                                   | **Description**                 | **Example JSON**                                                                          |
|                 | `200`                                                      | Returns everything              | [{"testcase_id": &lt;testcase_id&gt;,<br>"product_name": "&lt;product_name&gt;"},<br>...] |
|                 | `401`                                                      | There is no authorization token | {"status": 401,<br>"message": "You are not authorized to do this action."}                |
|                 | `500`                                                      | Internal server error           |                                                                                           |

| GET             | `/api/testcases/{id}`    |                                 |                                                                                                                                                                                                                                                                                                                                         |
|-----------------|--------------------------|---------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|                 | Get a testcase by its id |
| **Parameters:** | **Name**                 | **Type**                        | **Description**                                                                                                                                                                                                                                                                                                                         |
|                 | `token`                  | `auth`                          | Must be logged in to get a list                                                                                                                                                                                                                                                                                                         |
|                 | `id`                     | `path`                          | Testcase id to find                                                                                                                                                                                                                                                                                                                     |
| **Responses:**  | **Code**                 | **Description**                 | **Example JSON**                                                                                                                                                                                                                                                                                                                        |
|                 | `200`                    | Returns everything              | {"id": &lt;id&gt;,<br>"board_id": &lt;board_id&gt;,<br>"name_tester": "&lt;name_tester&gt;",<br>"name": "&lt;name&gt;",<br>"status": "&lt;status&gt;",<br>"duration": &lt;duration&gt;,<br>"due_date": &lt;due_date&gt;,<br>"weight": &lt;weight&gt;,<br>"description": "&lt;description&gt;",<br>"environment": "&lt;environment&gt;"} |
|                 | `401`                    | There is no authorization token | {"status": 401,<br>"message": "You are not authorized to do this action."}                                                                                                                                                                                                                                                              |
|                 | `404`                    | Testcase is not found           | {"error": "Test case not found"}                                                                                                                                                                                                                                                                                                        |
|                 | `500`                    | Internal server error           |                                                                                                                                                                                                                                                                                                                                         |

| GET             | `/api/testcases/{id}/steps`                   |                                 |                                                                                                                                          |
|-----------------|-----------------------------------------------|---------------------------------|------------------------------------------------------------------------------------------------------------------------------------------|
|                 | Get a list of all steps for a single testcase |
| **Parameters:** | **Name**                                      | **Type**                        | **Description**                                                                                                                          |
|                 | `token`                                       | `auth`                          | Must be logged in to get a list                                                                                                          |
|                 | `id`                                          | `path`                          | Testcase id to find                                                                                                                      |
| **Responses:**  | **Code**                                      | **Description**                 | **Example JSON**                                                                                                                         |
|                 | `200`                                         | Returns everything              | [{"id": &lt;id&gt;,<br>"testcase_id": &lt;testcase_id&gt;,<br>"description": "&lt;description&gt;",<br>"status": "&lt;status&gt;"}, ...] |
|                 | `401`                                         | There is no authorization token | {"status": 401,<br>"message": "You are not authorized to do this action."}                                                               |
|                 | `404`                                         | Testcase is not found           | {"error": "Steps of testcaseId ${testCaseId} not found!"}                                                                                |
|                 | `500`                                         | Internal server error           |                                                                                                                                          |

| GET             | `/api/testcases/{id}/products`                   |                                 |                                                                                           |
|-----------------|--------------------------------------------------|---------------------------------|-------------------------------------------------------------------------------------------|
|                 | Get a list of all products for a single testcase |
| **Parameters:** | **Name**                                         | **Type**                        | **Description**                                                                           |
|                 | `token`                                          | `auth`                          | Must be logged in to get a list                                                           |
|                 | `id`                                             | `path`                          | Testcase id to find                                                                       |
| **Responses:**  | **Code**                                         | **Description**                 | **Example JSON**                                                                          |
|                 | `200`                                            | Returns everything              | [{"testcase_id": &lt;testcase_id&gt;,<br>"product_name": "&lt;product_name&gt;"},<br>...] |
|                 | `401`                                            | There is no authorization token | {"status": 401,<br>"message": "You are not authorized to do this action."}                |
|                 | `404`                                            | Testcase is not found           | {"error": "Test case not found"}                                                          |
|                 | `500`                                            | Internal server error           |                                                                                           |

| GET             | `/api/testcases/{id}/bugreports`                 |                                 |                                                                                                                                                                                                                                                                                                                       |
|-----------------|--------------------------------------------------|---------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|                 | Get a list of all products for a single testcase |
| **Parameters:** | **Name**                                         | **Type**                        | **Description**                                                                                                                                                                                                                                                                                                       |
|                 | `token`                                          | `auth`                          | Must be logged in to get a list                                                                                                                                                                                                                                                                                       |
|                 | `id`                                             | `path`                          | Testcase id to find                                                                                                                                                                                                                                                                                                   |
| **Responses:**  | **Code**                                         | **Description**                 | **Example JSON**                                                                                                                                                                                                                                                                                                      |
|                 | `200`                                            | Returns everything              | [{"id": &lt;id&gt;,<br>"step_id": &lt;step_id&gt;,<br>"name_tester": "&lt;name_tester&gt;",<br>title": "&lt;title&gt;",<br>"description": "&lt;description&gt;",<br>"environment": "&lt;environment&gt;",<br>"product": "&lt;product&gt;",<br>"blocking": "&lt;blocking&gt;",<br>"status": "&lt;status&gt;"},<br>...] |
|                 | `401`                                            | There is no authorization token | {"status": 401,<br>"message": "You are not authorized to do this action."}                                                                                                                                                                                                                                            |
|                 | `500`                                            | Internal server error           |                                                                                                                                                                                                                                                                                                                       |

### POST Requests
| POST            | `/api/testcases/` |                                 |                                                                            |
|-----------------|-------------------|---------------------------------|----------------------------------------------------------------------------|
|                 | Create a testcase |
| **Parameters:** | **Name**          | **Type**                        | **Description**                                                            |
|                 | `token`           | `auth`                          | Must be logged in to add a testcase                                        |
|                 | `board_id`        | `body`                          | Id of the board this testcase belongs to                                   |
|                 | `due_date`        | `body`                          | Due date of the testcase                                                   |
|                 | `name`            | `body`                          | Name of the testcase                                                       |
|                 | `name_tester`     | `body`                          | Username of the tester assigned to this testcase                           |
|                 | `weight`          | `body`                          | Weight of the testcase                                                     |
|                 | `environment`     | `body`                          | Environment of the testcase                                                |
|                 | `description`     | `body`                          | Description of the testcase                                                |
|                 | `status`          | `body`                          | Status of the testcase                                                     | 
|                 | `product`         | `body`                          | Product(s) to which this testcase is related                               |
|                 | `steps`           | `body`                          | Steps of the testcase                                                      |
| **Responses:**  | **Code**          | **Description**                 | **Example JSON**                                                           |
|                 | `201`             | Successfully created a testcase | {"id": &lt;id&gt;}                                                         |
|                 | `401`             | There is no authorization token | {"status": 401,<br>"message": "You are not authorized to do this action."} |
|                 | `500`             | Internal server error           |                                                                            |

| POST            | `/api/testcases/{id}/products`                  |                                                  |                                                                            |
|-----------------|-------------------------------------------------|--------------------------------------------------|----------------------------------------------------------------------------|
|                 | Add a new relation between testcase and product |
| **Parameters:** | **Name**                                        | **Type**                                         | **Description**                                                            |
|                 | `token`                                         | `auth`                                           | Must be logged in to add a testcase                                        |
|                 | `testcase_id`                                   | `body`                                           | Id of the testcase                                                         |
|                 | `product_name`                                  | `body`                                           | Name of the product                                                        |
| **Responses:**  | **Code**                                        | **Description**                                  | **Example JSON**                                                           |
|                 | `201`                                           | Successfully created a testcase product relation | {"message": "Ok"}                                                          |
|                 | `401`                                           | There is no authorization token                  | {"status": 401,<br>"message": "You are not authorized to do this action."} |
|                 | `500`                                           | Internal server error                            |                                                                            |

### PUT Requests
| PUT             | `/api/testcases/{id}`    |                                         |                                                                            |
|-----------------|--------------------------|-----------------------------------------|----------------------------------------------------------------------------|
|                 | Edit a specific testcase |
| **Parameters:** | **Name**                 | **Type**                                | **Description**                                                            |
|                 | `token`                  | `auth`                                  | Must be logged in tto edit a testcase                                      |
|                 | `id`                     | `path`                                  | Testcase id to edit                                                        |
|                 | `board_id`               | `body`                                  | Id of the board this testcase belongs to                                   |
|                 | `due_date`               | `body`                                  | Due date of the testcase                                                   |
|                 | `name`                   | `body`                                  | Name of the testcase                                                       |
|                 | `name_tester`            | `body`                                  | Username of the tester assigned to this testcase                           |
|                 | `weight`                 | `body`                                  | Weight of the testcase                                                     |
|                 | `environment`            | `body`                                  | Environment of the testcase                                                |
|                 | `description`            | `body`                                  | Description of the testcase                                                |
|                 | `status`                 | `body`                                  | Status of the testcase                                                     |
|                 | `duration`               | `body`                                  | Time it took to do this testcase                                           |
| **Responses:**  | **Code**                 | **Description**                         | **Example JSON**                                                           |
|                 | `200`                    | Successfully updated a testcase         | {}                                                                         |
|                 | `401`                    | There is no authorization token         | {"status": 401,<br>"message": "You are not authorized to do this action."} |
|                 | `404`                    | The testcase with such id was not found | {"error": "Test case not found"}                                           |
|                 | `500`                    | Internal server error                   |                                                                            |

### DELETE Requests
| DELETE          | `/api/testcases/{id}`      |                                  |                                                                            |
|-----------------|----------------------------|----------------------------------|----------------------------------------------------------------------------|
|                 | Delete a specific testcase |
| **Parameters:** | **Name**                   | **Type**                         | **Description**                                                            |
|                 | `token`                    | `auth`                           | Must be logged in to delete a testcase                                     |
|                 | `id`                       | `path`                           | Testcase id to delete                                                      |
| **Responses:**  | **Code**                   | **Description**                  | **Example JSON**                                                           |
|                 | `204`                      | Successfully deleted a testcase  | {"message": "No content"}                                                  |
|                 | `401`                      | There is no authorization token  | {"status": 401,<br>"message": "You are not authorized to do this action."} |
|                 | `404`                      | There is no valid id in the path | {"error": "Bug report with ID not found!"}                                 |
|                 | `500`                      | Internal server error            |                                                                            |

| DELETE          | `/api/testcases/{id}/products/{product}`       |                                 |                                                                            |
|-----------------|------------------------------------------------|---------------------------------|----------------------------------------------------------------------------|
|                 | Delete a relation between testcase and product |
| **Parameters:** | **Name**                                       | **Type**                        | **Description**                                                            |
|                 | `token`                                        | `auth`                          | Must be logged in to delete a testcase                                     |
|                 | `id`                                           | `path`                          | Testcase id to delete                                                      |
|                 | `product`                                      | `path`                          | Product name to delete                                                     |
| **Responses:**  | **Code**                                       | **Description**                 | **Example JSON**                                                           |
|                 | `204`                                          | Successfully deleted a testcase | {"message": "No content"}                                                  |
|                 | `401`                                          | There is no authorization token | {"status": 401,<br>"message": "You are not authorized to do this action."} |
|                 | `500`                                          | Internal server error           |                                                                            |