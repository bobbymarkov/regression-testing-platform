# Functional Design

## Table Of Contents
1. [Introduction](#introduction)
   1. [Problem](#problem)
   2. [Context Analysis](#context-analysis)
2. [Solution Overview](#overview-solution)
3. [Functional Specs](#functional-specs)
   1. [Business Logic Mapping](#business-logic-mapping)
   2. [User Stories](#user-stories)
   3. [Wireframes](#wireframes)
   4. [Functional Requirements](#functional-requirements)
4. [System Configurations](#system-configurations)
5. [Non-functional Requirements](#non-functional-requirements)
6. [Exception Handling](#exception-handling)
7. [Change Log](#change-log)

## Introduction
In the century of the technological innovation and people development, Scorion stands as a progressive and creative company.
Company is located in Deventer and has a lot of passion for people, development, innovation and technology.
This drive is embodied in the main mission: to empower others through the innovative online applications, enabling them to
map and analyze innovation and development data for further stimulation and organization.
### Problem
Despite the advanced development inside the company, Scorion faced the challenge in the current regression test management system.
So far, they are using the scrum board in JetBrain Space environment to monitor regression test throughput for the products.
However, the system lacks efficient communication for reporting blockers and bugs. Furthermore, the system falls short
in tracking test progression and notifying testers about upcoming or overdue tests.
### Context Analysis
In current scenario, Scorion's development and testing teams are challenged in the existing regression test management
system within the JetBrains Space environment. Key issues include inefficient communication, not convenient tracking
and reporting of test progress. This analysis highlights are helpful to building the effective system, which will cover all
the needs.
***

## Solution Overview
To solve the problem, the project went to our team. Our aspiration is to develop a comprehensive
regression testing management system focused on manual testing by real people. This system will allow
administrators to manage the overall configuration, including adding and removing test cases for sprints.
Testers will work with the system to check test cases, provide detailed bug reports, and update the contents
of test cases as necessary.

***
## Functional Specs
### Business Logic Mapping

**Objective:**
Develop a comprehensive regression testing management system focusing on manual testing.
The system aims to enhance communication, reporting, and progress tracking for Scorion's products (ScorionX, PIP, Groeidocument, and EasionX).

**Key Components:**

1. **Admin Configuration:**
  - Admins manage overall configuration.
  - Ability to reset/prepare a new board for testing each sprint.
  - Admins can add a new tester to the system, and reset user's password.

2. **Tester Functionality:**
  - Testers execute and update test cases in a regression test run.
  - Provide bug reports linked to specific test cases.

3. **Communication:**
  - Notification to relevant individuals about assigned tests.

4. **Reporting:**
  - Reporting of completed regression tests per sprint and product.
  - Progression tracking based on completed weight and finished date.
  - Notifications to testers about upcoming or overdue tests.

5. **Progress Monitoring:**
  - Tracking and recording start and end times of a test walkthrough.
  - Calculation of test walkthrough duration and expected end time.

**Additional Considerations:**
- Role-based access control for test round preparation.
- Support for attachments and accounts needed for tests.
- Notification system for informing relevant individuals about blockers and test progress.

**Overall Goal:**
Enhance Parantion's manual regression testing process by providing a user-friendly and efficient management system, addressing communication challenges, improving reporting and progress tracking, 
and accommodating the specific needs of their organization and products.


### User Stories

The following contains a list of the User Stories, for detailed test descriptions check the issues in the backlog on GitLab.

![userStories1.png](..%2FParantion-WF%2FuserStories1.png)
![userStories2.png](..%2FParantion-WF%2FuserStories2.png)
![userStories3.png](..%2FParantion-WF%2FuserStories3.png)

### Wireframes
![Parantion-WF-1.png](..%2FParantion-WF%2FParantion-WF-1.png)
*Illustration 1: Login screen*

This wireframe displays a login page for the website. The functionality of this page is straightforward:

- Users can enter their email address in the first field to identify their account.
- Then, they input their password in the second field to verify their identity.
- The 'Login' button is used to submit the details and access their account.
- In case of wrong credentials error message will be shown

![Parantion-WF-2.png](..%2FParantion-WF%2FParantion-WF-2.png)
*Illustration 2: View board screen and notification*

This wireframe shows a "View Board" page, for a tester's tasks view. The functionality of this page includes:

- A column on the left for "Filter" where testers can select product they are going to test.
- The main area of the page is divided into four columns: "In-progress," "OK," "Bugs," and "Blockers." Each column represents a different status of tasks:
  - "In-progress" shows tasks that are created but not assigned.
  - "Pending"  represents tests that are being conducted.
  - "Done" are tests that are fully finished.
  - "Failed" are tests that are failed.
- Inside each column are boxes representing individual tests. Upon clicking on it, the tester will be redirected to a page of test details.
- In the top right corner, there are notifications for the testers about due tasks and assignments, providing quick access to their priorities.

Overall, this board provides an overview of the project's current status and allows the tester to manage and track their work efficiently.

![Parantion-WF-6.png](..%2FParantion-WF%2FParantion-WF-6.png)
*Illustration 3: All boards screen*

The wireframe above shows an "All Boards" page, which is part of an admin's dashboard for a test tracking system. Here's the functionality this page provides:

- The page lists all available project board. Upon clicking a board, it leads to a more detailed view of the board.
- There are navigation links or tabs at the top labeled "Boards", "Reports", "Users", and "Account", meaning that the admin has access to different sections for managing various aspects of the system.
- A search bar is provided, allowing the admin to quickly find a specific board among potentially many based on the name of the board.
- Board can also be filtered based on status: In-Progress, Nearing deadlines, or Finished.
- The "+ Add new board" button implies that the admin can create new boards as needed.

Overall, this page is designed to give administrators a high-level overview of all the boards they have access to, providing a centralized place to navigate to different areas within the system.

![Parantion-WF-3.png](..%2FParantion-WF%2FParantion-WF-3.png)
*Illustration 4: Testcase screen for admin*

If the user is logged in as an admin, they can modify the testcase.
- On the left side, the admin can choose to add/edit the description, add the steps required for the test. They can also add comments related to the test.
- On the right side, the admin have the option the add weight and add due date for the test. The admin can also add or remove responsible testers via the tags input box.
- The environment and product is also clarified here, admin can choose which environment or product via corresponding dropdown box.


![Parantion-WF-4.png](..%2FParantion-WF%2FParantion-WF-4.png)
*Illustration 5: Testcase screen for tester*

This screen shows the UI for a tester. Similar to the admin, tester can see the essential fields for a testcase. 
- Testers do not have the power to edit any field.
- However, testers can add comments related to the testcase, choose the result from dropdown box, and add Bug report via clicking the [ + ] button next to the steps.
- Once the tester was assigned and subsequently accepted the test, they can click the [ Start ] button to start recording the time spent on the testcase.

![Parantion-WF-5.png](..%2FParantion-WF%2FParantion-WF-5.png)
*Illustration 6: Board report screen for a single board*

This screen displays a table with data record for a test board. 
- The admin can choose which board to display via the dropdown box. 
- A single report is a table with list of testcases in the board, with test ID, total time taken to finish, due date, actual date, and weight.
- User can choose the filter the product by the filter sidebar on the left side.  
- Board report can be exported to PDF or CSV via clicking on the buttons.


![Parantion-WF-7.png](..%2FParantion-WF%2FParantion-WF-7.png)
*Illustration 7: Adding new user, changing username and password*

If an admin is logged in, they have the option to create a user for the system. 
- If the user already exists in the system, and error message will also be displayed.
- The admin can create a user based on email, and they can also generate password for the user. 
- The user can be assigned role (either Admin or Tester) via the dropdown box.
- Existing user can be edited by clicking on the radio button and change username or clicking change password.
- If the admin chooses to change user password, an email will be sent to the email registered to the user.

![Parantion-WF-8.png](..%2FParantion-WF%2FParantion-WF-8.png)
*Illustration 8: Change password*

If the admin chose to change an user's password and the user clicked on the change password email sent to them, they will
be redirected to a change password screen.
- The user will be asked to enter the new password and enter it again in confirm password input field.
- The user can choose to save their new password with the [ Save ] button.
- After saving, the user will be redirected to Login page to log in with their new password.

![Parantion-WF-10.png](..%2FParantion-WF%2FParantion-WF-10.png)
*Illustration 9: Create/edit a bug report*

If the user encounters a bug at any given step in a test case, they can choose to create a bug report.
- On the left side of the screen is title, description to reproduce the bug.
- Attachments can be added to visualize the bug.
- On the right side is information of the test case with assigned tester, product, environment.
- The bug can be marked as Resolved or Not resolved, based on its own status or the current status of the step and testcase.

![Parantion-WF-9.png](..%2FParantion-WF%2FParantion-WF-9.png)
*Illustration 10: View assigned testcases*

If the user clicks on account navigation on the header, they can view all assigned tests.
- Testcase is displayed in a colour coded container.
- Gray: Testcase is Not started, Yellow: testcase is Pending, Red: Failed, and Green: Done.

### Functional Requirements

| ID   | Requirement                                                                                                                                                                                                                             | MoSCoW | Source                 |
|------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------|------------------------|
| F-1  | The system must provide a login function for different user roles.                                                                                                                                                                      | M      | UR-1                   |
| F-2  | The system must provide an interface for admin users to create a new board for a new sprint. Admin users should be able to specify the board details, including name and due date, during the creation process.                         | M      | UR-2                   |
| F-3  | The system should provide an interface for admin users to manage different boards. Each sprint boards should be viewable, editable in terms of the board details.                                                                       | M      | UR-4, UR-5             |
| F-4  | The system must provide an interface to manage all test cases inside each board. Admin should then be allowed to add, view, edit, delete test cases.                                                                                    | M      | UR-6, UR-7, UR-8, UR-9 |
| F-5  | The system must permit admin users to add, edit, delete steps of test cases.                                                                                                                                                            | M      | UR-10, UR-11, UR-12    |
| F-6  | The system must provide a visual representation of sprint progress based on the completed weight, time, and number of bugs of tests. Admin users should have access to a graphical representation to easily comprehend sprint progress. | M      | UR-13, UR-14, UR-15    |
| F-7  | The system should provide admin users with a tool or interface to export a regression test report linked to a specific board.                                                                                                           | C      | UR-16                  |
| F-8  | The system should allow admin users to modify the roles of other users, adjusting their level of access within the project.                                                                                                             | S      | UR-17                  |
| F-9  | The system should only allow admin users to assign testers to test cases                                                                                                                                                                | M      | UR-18                  |
| F-10 | The system should provide an interface for each tester to view all the test cases they are assigned to.                                                                                                                                 | M      | UR-19                  |
| F-11 | The system should permit tester users to update the status of the steps in their assigned test cases.                                                                                                                                   | S      | UR-20                  |
| F-12 | The system should update the status of test cases automatically based on the status of the steps in each of the test case.                                                                                                              | M      | UR-21                  |
| F-13 | The system should have a feature allowing tester users to add, view, edit the bug report of a test case.                                                                                                                                | M      | UR-22, UR-23, UR-24    |
| F-14 | The system should remove all the bug reports automatically inside a test case when all the bugs inside it have been fixed.                                                                                                              | S      | UR-25                  |
| F-15 | The system should notify related testers of a test case 3 days prior to the due date.                                                                                                                                                   | M      | UR-26                  |
| F-16 | The system should create comment section on each test case for related users to discuss on there.                                                                                                                                       | C      | UR-27                  |

***

## System Configurations
### Project setup
**Intent**
- Initialize a Vite + Svelte project with Node.js/Express back end.

**Steps**
- Install Node.js via the command npm install
- Use command line to scaffold a Vite + Svelte project, run: npm create vite@latest my-vue-app -- --template svelte
- Create a new Vite project with Svelte template.
-  Install and configure the "page" library for client-side routing.
-  Use "validate.js" to validate user input and JSON data.
-  Include "font-awesome" for icon integration.
-  Import Google Fonts using "google-font" for custom typography.
-  Utilize "jspdf" for generating PDF documents in the browser.
-  Manage configurations using the "dotenv" library.

**Alternative**
- Due to the requirements of the assignment, Svelte is the only possible choice.
- Other libraries for icons and styling, for example Bootstrap, MaterialUI...

**Dependencies**
- Node.js and npm installation

### User Authentication and Email Notifications
**Intent**
- Implement add new user function with admin role and login functionality.

**Steps**
- Create registration and login pages using "page" for routing.
- Use "validate.js" to validate user input during registration.
- Secure user passwords using "bcrypt" for encryption.
- Integrate "nodemailer" to send emails from the server to the user email.

**Alternatives/Customizations**
- Implement social login (e.g., OAuth) for user authentication.
- Customize the user registration and login process. 

**Dependencies**
- Frontend libraries and backend API endpoints, "nodemailer".

### PDF Generation
**Intent**
- Allow users to generate PDF documents within the application.

**Steps**
- Integrate "jspdf" library to create PDF documents on the client side.
- Provide a user interface for generating and downloading PDFs.

**Alternatives/Customizations**
-Use server-side PDF generation services (e.g., Puppeteer) for complex PDFs.
- Customize PDF templates and layouts.

**Dependencies**
- Svelte, "jspdf."

### File Upload and Storage
**Intent**
- Implement file upload functionality and storage for attachments.

**Steps**
- Use "multer" middleware to handle file uploads in Express.js.
- Integrate "aws-sdk" for storing file attachments in Amazon S3 (or another cloud storage service).

**Alternatives/Customizations**
- Choose a different cloud storage provider (e.g., Azure Blob Storage).
- Implement local file storage.
- Store file in PostgresSQL database.

**Dependencies**
- Express, "multer," "aws-sdk."
***
## Non-functional Requirements

| ID              | Requirement                                                                                                                                                                                                                              | MoSCoW | Source    |
|-----------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------|-----------|
| NF-1            | The system is required to store all data in a separate database.                                                                                                                                                                         | M      | Interview |
| NF-2            | The system is required to maintain the confidentiality, integrity, and availability of all data and transactions, ensuring compliance with GDPR legislation. Unauthorized access, modification, or disclosure of data must be prevented. | M      | Interview |
| NF-3            | The system's design should be ready for the company's future growth by accommodating additional features, functionalities, and increased data storage capacity.                                                                          | S      | Interview |
| NF-4            | The system is expected to maintain an availability level of 96.67%.                                                                                                                                                                      | M      | Interview |
| NF-5            | The system could be scalable.                                                                                                                                                                                                            | C      | Interview |
| NF-6            | The system is expected to respond within 2 seconds for any user action, even under normal load conditions (except attaching different file format).                                                                                      | M      | Interview |
| NF-7            | Maintenance of the system should be straightforward, enabling the implementation of updates and bug fixes without causing downtime or data loss.                                                                                         | M      | Interview |
| NF-8            | A robust backup and disaster recovery plan must be in place to guarantee the restoration of all data in case of a system failure or data loss.                                                                                           | S      | Interview |
***
## Test cases


| Test case ID      |                          TC-1                          |
|:------------------|:------------------------------------------------------:|
| Name              |         Different rights for the product owner         |
| Short description | The user should be able to log in with different roles |
| Requirement       |                          US-1                          |

**Preconditions:**
The user has opened the application.

| Step   |                         Action                          |                                                   Expected outcome                                                    | Pass/Fail |
|:-------|:-------------------------------------------------------:|:---------------------------------------------------------------------------------------------------------------------:|:---------:|
| 1      |              Enter the username for tester              |                                     The input field with username holds the input                                     |   Pass    |
| 2      |              Enter the password for tester              |                              The input field for password holds the input in safe format                              |   Pass    |
| 3      |                  Press button "Login"                   |                 The user enters the system with tester role: only tab Account available in the header                 |   Pass    |
| 4      | Press button "Logout" in the right corner of the header |                                  The user was logged out and sent to the login page                                   |   Pass    |
| 5      |              Enther the username for admin              |                                                      See step 1                                                       |   Pass    |
| 6      |              Enter the password for admin               |                                                      See step 2                                                       |   Pass    |
| 7      |                  Press button "Login"                   | The user enters the system with admin role: there are multiple tabs in the header including Boards, Users and Account |   Pass    |

**Postcondition:**
The user has entered the application with corresponding role.


| Test case ID      |                               TC-2                               |
|:------------------|:----------------------------------------------------------------:|
| Name              |                    Manipulations with boards                     |
| Short description | The user should be able to view/add/duplicate/edit/delete boards |
| Requirement       |                         US-3, US-4, US-5                         |

**Preconditions:**
The user is logged in as admin.

| Step |                                   Action                                   |                                            Expected outcome                                             | Pass/Fail |
|:-----|:--------------------------------------------------------------------------:|:-------------------------------------------------------------------------------------------------------:|:---------:|
| 1    |                              Go to Board page                              |                                         The Board page is open                                          |   Pass    |
| 2    |                          Click on "Add new board"                          |                                  The pop-up with input fields is shown                                  |   Pass    |
| 3    |                    Fill in needed data and press "Save"                    |                              The new board is now shown on the Board page                               |   Pass    |
| 4    |           Click on the dots in the top right corner of the board           |                                          The options are shown                                          |   Pass    |
| 5    |                             Click "Duplicate"                              |                                               See step 2                                                |   Pass    |
| 6    |                               Perform step 3                               |  The duplicated board is now shown on the Board page with all the testcases set to Not started status   |   Pass    |
| 7    |          Perform step 5 and choose "Edit" and fill in needed data          |                             The board is now saved with updated information                             |   Pass    |
| 8    | Perform step 5 and choose "Delete" and confirm the action on alert pop-up  |                  The board has been deleted and therefore is missing on the Board page                  |   Pass    |
| 9    |                             Click on the board                             |                     The page of specific board is open with all its testcases shown                     |   Pass    |

**Postcondition:**
The user is able to perform different manipulations with the board.


| Test case ID      |                                         TC-3                                          |
|:------------------|:-------------------------------------------------------------------------------------:|
| Name              |                             Manipulations with testcases                              |
| Short description |               The user should be able to view/add/edit/delete testcases               |
| Requirement       | US-6, US-7, US-8, US-9, US-10, US-11, US-12, US-13, US-18, US-19, US-20, US-21, US-27 |

**Preconditions:**
The user is logged in as admin. Please note: the steps with note (tester) can be performed by tester as well.

| Step  |                    Action                     |                                        Expected outcome                                         | Pass/Fail  |
|:------|:---------------------------------------------:|:-----------------------------------------------------------------------------------------------:|:----------:|
| 1     |        Go to the Account page (tester)        | All the testcases assigned to the user are represented there in the colour based on the status  |    Pass    |
| 2     |        Click on the testcase (tester)         |                                 The testcase data is displayed                                  |    Pass    |
| 3     |         Fill in description (tester)          |                             The description input field holds input                             |    Pass    |
| 4     |       Change status of all step Failed        |                                All the steps have status Failed                                 |    Pass    |
| 5     |                 Press “Save”                  |                           The confirmation pop-up has been displayed                            |    Pass    |
| 6     |            Go back to Account page            |                             The testcase’s border is now set to red                             |    Pass    |
| 7     |          Click on the testcase again          |                       The changes are displayed in the information fields                       |    Pass    |
| 8     |       Change status of all steps to OK        |                                 All the steps have status "OK"                                  |    Pass    |
| 9     |                 Repeat step 5                 |                                                                                                 |    Pass    |
| 10    |                 Repeat step 6                 |              The testcase has been removed from the Account page due to the status              |    Pass    |
| 11    |               Go to Board page                |                               All the boards have been displayed                                |    Pass    |
| 12    |                 Choose board                  |                        All the testcases of the board is being displayed                        |    Pass    |
| 13    |   Drag testcase from Not started to Pending   |                       The testcase has been moved to the column “Pending”                       |    Pass    |
| 14    |               Click on testcase               |                                 The testcase data is displayed                                  |    Pass    |
| 15    |       Change name, fill in description        |                                 The input fields hold the input                                 |    Pass    |
| 16    |  Click add step and fill in the description   |                           The confirmation pop-up has been displayed                            |    Pass    |
| 17    |       Set the status of all steps to OK       |                                  All the steps have status OK                                   |    Pass    |
| 18    | Change the date, product, environment, tester |                           The fields hold the changes correspondingly                           |    Pass    |
| 19    |                 Press “Save”                  |                           The confirmation pop-up has been displayed                            |    Pass    |
| 20    |                    Go back                    |                        The testcase has been moved to the column “Done”                         |    Pass    |
| 21    |          Click on the testcase again          |                       The changes are displayed in the information fields                       |    Pass    |
| 22    |                Press “Delete”                 |                           The confirmation pop-up has been displayed                            |    Pass    |
| 23    |                    Go back                    |                           The testcase has been removed from the page                           |    Pass    |

**Postcondition:**
The user is able to perform different manipulations with the testcases.

| Test case ID      |                            TC-4                             |
|:------------------|:-----------------------------------------------------------:|
| Name              |                Manipulations with bug report                |
| Short description | The user should be able to view/add/edit/delete bug report  |
| Requirement       |                 US-22, US-23, US-24, US-25                  |

**Preconditions:**
The user is logged in tester/admin.

| Step  |                     Action                      |                                 Expected outcome                                  | Pass/Fail  |
|:------|:-----------------------------------------------:|:---------------------------------------------------------------------------------:|:----------:|
| 1     |             Go to the Account page              |                         The testcases have been displayed                         |    Pass    |
| 2     |                Click on testcase                |                       The testcase data has been displayed                        |    Pass    |
| 3     |       Click on bug icon next to the step        |                      The Bug report page has been displayed                       |    Pass    |
| 4     |          Fill in name and description           |                            The input fields hold input                            |    Pass    |
| 5     |                Click attach file                |                           The file explorer been opened                           |    Pass    |
| 6     |             Choose file on your pc              |                   The file has been attached to the bug report                    |    Pass    |
| 7     |                  Press “Save”                   |                           The bug report has been saved                           |    Pass    |
| 8     |                     Go back                     | The created bug report has been displayed in the right bottom corner of the page  |    Pass    |
| 9     | Click on bug report in the right bottom corner  |                        The updated data has been displayed                        |    Pass    |
| 10    |                 Press “Delete”                  |                            The bug report been deleted                            |    Pass    |
| 11    |                     Go back                     |              The bug report disappeared from the right bottom corner              |    Pass    |

**Postcondition:**
The user is able to perform different manipulations with the bug report.

| Test case ID      |                          TC-5                           |
|:------------------|:-------------------------------------------------------:|
| Name              |                Viewing the board report                 |
| Short description |  The user should be able to view/download board report  |
| Requirement       |               US-13, US-14, US-15, US-16                |

**Preconditions:**
The user is logged in as admin. 

| Step  |                     Action                     |                                         Expected outcome                                         | Pass/Fail  |
|:------|:----------------------------------------------:|:------------------------------------------------------------------------------------------------:|:----------:|
| 1     |            Go to the specific board            |                        All the testcases of the board have been displayed                        |    Pass    |
| 2     |              Press “Board report”              |                    The board report with all analysis data has been displayed                    |    Pass    |
| 3     |             Press “Export to PDF”              |                     The pdf file with corresponding data has been downloaded                     |    Pass    |
| 4     |             Press “Export to CSV”              |                     The csv file with corresponding data has been downloaded                     |    Pass    |

**Postcondition:**
The user is able to view and download board report.

| Test case ID      |                            TC-6                             |
|:------------------|:-----------------------------------------------------------:|
| Name              |                  Manipulations with users                   |
| Short description | The user should be able to view/edit/add/delete other users |
| Requirement       |                            US-17                            |

**Preconditions:**
The user is logged in as admin.

| Step   |                      Action                      |                                         Expected outcome                                          |  Pass/Fail  |
|:-------|:------------------------------------------------:|:-------------------------------------------------------------------------------------------------:|:-----------:|
| 1      |               Go to the User page                |                                 All the users have been displayed                                 |    Pass     |
| 2      |                   Choose user                    |                               The user radio button has been chosen                               |    Pass     |
| 3      |                   Edit fields                    |                                   The fields contain input data                                   |    Pass     |
| 4      |                   Press “Save”                   |                                 The user’s data has been updated                                  |    Pass     |
| 5      |                  Repeat step 2                   |                                                                                                   |    Pass     |
| 6      |                  Press “Delete”                  |                                The user disappeared from the list                                 |    Pass     |
| 7      |             Press “Create new user”              |                          The pop-up with input fields has been displayed                          |    Pass     |
| 8      |                   Fill in data                   |                                   The fields contain input data                                   |    Pass     |
| 9      |                   Press “Save”                   |                             The new user has been added to the field                              |    Pass     |

**Postcondition:**
The user is able to perform manipulations with other users accounts.

## Exception Handling
| ID  | Error            | Cause                                                               | Solution Strategy                                                                                       |
|-----|------------------|---------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------|
| 1   | Input validation | The user enters wrong input format                                  | Error description in JSON format for backend and Error pop-up alert in fronend                          |
| 2   | Network issue    | System configuration in env file are incorrect                      | A dotenv guide is written in documentation/guides to provide dummy data to access the server/database   |
| 3   | Database error   | The user input incompatible data for table                          | Parameterized queries in back end for database queries used for API calls, try-catch block in API calls |
| 4   | REST API         | API calls do not reach the right endpoints, endpoints are not setup | Import HTTP status codes to respond with appropriate status code                                        |
***

## Change Log

| Date       | Version | Document Revision Description        | Document Author |
|------------|---------|--------------------------------------|-----------------|
| 03-12-2023 | 0.1     | Initial document                     |                 |
| 21-01-2023 | 0.2     | System configuration, Error handling | Trang Tran      |
| 28-01-2023 | 0.3     | Change wireframes + description      | Trang Tran      |
|            |         |                                      |                 |
|            |         |                                      |                 |
|            |         |                                      |                 |
|            |         |                                      |                 |
