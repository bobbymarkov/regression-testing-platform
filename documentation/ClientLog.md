# Client log

Communication with the client will be recorded in this file with the date, relevant client representative, the topic/deliverables, and result.
Direct contact with the client is also maintained via the discord group. 

| Date        | Client representative | Topic/Deliverable                        | Result                                                   |
|-------------|-----------------------|------------------------------------------|----------------------------------------------------------|
| 28/11/2023  | Parantion             | First client meeting                     | Introduction to the project. Answers to questions.       |
| 30/11/2023  | Joran                 | Draft of the wireframes and user stories | Minor feedback, more feedback will be provided on Monday |

## Client meeting 28/11/2023
This is the first client meeting, in which the client prepared a short presentation, introduced the supervisors for the projects,
and explaining some requirements to the group. The group has prepared questions before the meeting and taken notes:

- The system should have multiple user roles: admin and user. They can be hardcoded.
- It should be possible to create, delete, and modify test cases.
- Issue must be resolved before the next Sprint.
- Notification of upcoming or overdue tests are up to the group to decide.
- Testers are not allowed to modify board or test cases.
- Reports should be exported in excel, csv, pdf...

## Contact during Sprint 1
During Sprint 1, the group maintained contact with Joran (Parantion representative) via Discord.
Questions that were asked and answered:
- Client's requirement on what a bug report should represent/contain.
- Definition of a board in relation with a Sprint.
- Bug report deletion.

The group arranged a meeting with the client via email on 12/12/2023.

## Client meeting 19/12/2023

After Sprint 1, the group arranged a meeting with the client on 19/12/2023.
The result of the Sprint 1 can be seen in Sprint 1 retrospective and Gitlab issue boards.
The group has noted down feedback from the client, notably:

- Put radio button on the left of the username at ‘Users’ page.
- On account view which includes testcases that are assigned to a user, show
test case status and title.
- Testcases should have better identifiers, either via colour or other design choices.
- User password should be randomised at creation, and admin should not know it at any time
 but be able to reset again.
- Create a password reset page, that is only accessible by unique link sent to the user.
- Admin should have access to every functionality in the system.
- Admin should also be able to run a testcase.
- Header should be resized.
- Reconsider the progress for Board and Test case.
- Board, test case, step, and bug report should have a dynamic relationship:
if status in either of those entity is changed to Failed or OK, others should be automatically changed.

Overall, the client was open to giving feedback, especially feedback regarding the overall appearance of the application.

## Contact during Sprint 2
The group did not have much trouble with implementing the system during Sprint 2. Most questions were already answered during the client meeting.
Question that was asked and answered on Discord:
- File uploading - the client gives the group freedom to choose the method they are most comfortable with, but preferably a local file storage, or database.

The group arranged a meeting with the client via email on 09/01/2024.

## Client meeting 11/01/2024

After Sprint 2, the group arranged a meeting with the client on 11/01/2024.
The result of the Sprint 1 can be seen in Sprint 2 retrospective and Gitlab issue boards.
The client (Joran) was satisfied with the current progress of the project and remarked
on the fact that most core functionalities are already implemented. 
The group has written down feedback from the client as follows:

- Instead of exporting to PDF (which is fully implemented), exporting to CSV should be prioritized.
- H1 title for some pages should be restyled to the left side of the screen.
- File from bug report should be able to be removed.
- If there is no bug report, the bug report overview section in test case shouldn't be displayed.
- Header and footer should have the same colour.
- Input validation should be checked more thoroughly.
- Styling for some buttons in test case.
- Board report should be able to export both products.
- Add product column in bug report.
- Radio button for filter should be check boxes to enabling choosing both products.
- Board duplication functionality.
- Users page still needs better styling.

Since the group has managed to implemented most of the main functionalities, most of the remarks
from the client this time are about styling. 
Sprint 3 will be spent on merging every working functions together and ensure that there is no conflict.

## Client meeting - demo demonstration 25/01/2024
The group prepared a short demo for the client. The feedback was very positive.
However, there were some minor feedback as below:
- Video recording should be prepared in case the demo does not go as intended or in case of failed internet connection.
- Any user confirmation action needs to trigger an alert.

