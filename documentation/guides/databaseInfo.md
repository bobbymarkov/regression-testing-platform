## Table

Account

| Property | Admin | Tester | Tester  |                                                                                                                                     
|----------|-------|--------|---------|
| username | admin | tester | tester1 |                                        
| password | admin | tester | tester1 |                                                   
| is_admin | true  | false  | false   |

Board

| Property      | Data     |                                                                                                                                         
|---------------|----------|
| id            | admin    |                                           
| Name          | password |                                                    
| creation_date | true     |

Testcase

| Property      | Data     |                                                                                                                                         
|---------------|----------|
| id            | admin    |                                           
| Name          | password |                                                    
| creation_date | true     |