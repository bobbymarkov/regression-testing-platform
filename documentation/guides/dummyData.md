INSERT INTO account (username, email, password, is_admin)
VALUES
('admin', 'john_doe@email.com', '$2a$12$3hwWLozX5Re7rJ47zWqr6uoxYXH1MqEHk1QRP6M7ybNVErthtKxvu', true),
('tester', 'alice_smith@email.com', '$2a$12$wUMyGaikh5IWYdygb/5b8OQo/MBUHAY2TtJdH1bpmiu4LcGVCeWbG', false),
('tester2', 'bob_jones@email.com', '$2a$12$mc0Lnbex8R2c4xDAeqmGB.aUSnynRLIXCuLiSCSpDP0RkmlAX7.r2', false);

INSERT INTO board (name, due_date)
VALUES
('First Project', '2024-01-27'),
('Second Project', '2024-12-12'),
('Third Project', '2023-11-12');

INSERT INTO testcase (board_id, name, status, duration, due_date, weight, description, environment, name_tester)
VALUES (1, 'Product Search', 'Not Started', INTERVAL '25 minutes','2024-01-27', 7, 'Test search functionality on the product page', 'Web', 'alice_smith'),
(1, 'Add to Cart', 'Pending', INTERVAL '15 minutes', '2024-01-27', 6, 'Verify the ability to add items to the shopping cart', 'Web', 'bob_jones'),
(1, 'Checkout Process', 'Not Started', INTERVAL '40 minutes','2024-01-27', 9, 'Test the end-to-end checkout process', 'Web', 'alice_smith'),
(2, 'File Upload', 'Pending', INTERVAL '30 minutes', '2024-12-12', 8, 'Verify file upload functionality', 'Web', 'bob_jones'),
(2, 'User Profile Management', 'Not Started', INTERVAL '20 minutes', '2024-12-12', 5, 'Test updating user profile information', 'Web', 'alice_smith'),
(2, 'Review and Rating', 'Pending', INTERVAL '15 minutes', '2024-12-12', 7, 'Test user review and product rating features', 'Web', 'bob_jones');

INSERT INTO product (name)
VALUES ('Easion'),
('Scorion');

INSERT INTO testcase_product (testcase_id, product_name)
VALUES (1, 'Easion'),
(1, 'Scorion'),
(2, 'Scorion'),
(3, 'Scorion'),
(4, 'Easion'),
(5, 'Easion');

INSERT INTO step (description, testcase_id, status)
VALUES
('Verify successful login message is displayed', 1, 'N/A'),
('Check user dashboard after login', 1,'N/A'),
('Verify confirmation email is sent after registration', 2,'N/A'),
('Check database for new user record after registration', 2,'N/A'),
('Click on "Forgot Password" link and follow the reset process', 3,'N/A'),
('Verify error message for incorrect login credentials', 3,'N/A');

