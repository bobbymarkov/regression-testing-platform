# Dotenv file for environmental variables

Step to create and use a dotenv file:
1. Run: npm install dotenv on both client and server side
2. In your client and server directory, create a new file named .env
3. Store variables in the file in this format:

![img.png](img.png)

<span style="color:red">Note:</span>
For client side, the variable name has to start with prefix VITE_, example:

![img_2.png](img_2.png)

3. <span style="color:red">IMPORTANT:</span>
   : in .gitignore, add:
   *.env to ignore all files with the .env extension


## Server side
Step and location where .env configs are used:
### index.js

1. In .env, enter the port variable: PORT: 3000
2. import 'dotenv/config'
3. (This is an example, the port is already configured in the project)
      Enter the port according to the dotenv file:

![img_1.png](img_1.png)


### s3_uploadfunc.js
1. In .env, enter the following variables for the S3 account used for the project:

| KEY                | VALUE                                      | 
|--------------------|--------------------------------------------|
| ACCESS_KEY_ID:     | 'AKIA2UC3BWRSAUUDWS6A'                     |
| SECRET_ACCESS_KEY: | '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' | 
| REGION:            | 'eu-central-1'                             | 

2. import 'dotenv/config'

### databaseHelper.js
1. In .env, enter the following variables for the Database used for the project:

| KEY                | VALUE           |
|--------------------|-----------------|
| DATABASE_USER:     | 'dbuser'        |
| DATABASE_HOST:     | 'localhost'     |
| DATABASE:          | 'ClientOnBoard' |
| DATABASE_PASSWORD: | 'user'          |
| DATABASE_PORT:     | 5432            |

2. import 'dotenv/config'

### All current values for convenient copy-paste:

PORT: 3000

DATABASE_USER: 'dbuser'

DATABASE_HOST: 'localhost'

DATABASE: 'ClientOnBoard'

DATABASE_PASSWORD: 'user'

DATABASE_PORT: 5432

ACCESS_KEY_ID: 'AKIA2UC3BWRSAUUDWS6A'

SECRET_ACCESS_KEY: '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'

REGION: 'eu-central-1'


## Client side
Step and location where .env configs are used:
1. Enter into .env file: VITE_BASE_URL: 'http://localhost:3000'
2. At this step, using the variables is different from server side.
Wherever fetch API needs to be used, use import.meta.env.VITE_BASE_URL
since process.env is removed from Vite.

### All current values for convenient copy-paste:
VITE_BASE_URL: 'http://localhost:3000'
