# Daily Scrum

# Introduction

Welcome to group 1 – DHI2V.Sp daily scrum board. This is where all stand-up meetings and retrospective across our three
working sprints are documented. For each sprint, we will have 4 stand-up meetings on Tuesday and Thursday each week –
same with our learning schedule – to help members keeping track with others’ working progress.

Scrum master for each sprint:

    Sprint 0: Are

    Sprint 1: Trang

    Sprint 2: Bach

    Sprint 3: Bobby

## Sprint 0

### 30.11.2023

|        | Done                                                                     | Problems                               | Will do                                                               |
|--------|--------------------------------------------------------------------------|----------------------------------------|-----------------------------------------------------------------------|
| Are    | started with system design, frameworks, libraries(from web)              | question about frameworks will use     | finish system design                                                  |
| Bach   | requirements                                                             | had some question about 2 requirements | put all the requirements in a table and sort them in MoSCoW principle |
| Bobby  | requirements                                                             | had some question about 2 requirements | put all the requirements in a table and sort them in MoSCoW principle |
| Kamila | absent                                                                   |                                        |                                                                       |
| Koray  | stakeholders, problem and context description (plan of approach)         | none                                   | finish his part of functional requirements                            |
| Lisa   | finished the wireframes                                                  | none                                   | revise and write description for wireframes                           |
| Sasha  | working on the functional design document and finished the roles for PoA | none                                   | Continue working on the functional design                             |
| Trang  | finished the wireframes                                                  | none                                   | revise and write description for wireframes                           |

## Sprint 1

### 05.12.2023

|        | Done                                                                              | Problems                                        | Will do                                                 |
|--------|-----------------------------------------------------------------------------------|-------------------------------------------------|---------------------------------------------------------|
| Are    | system design, architecture, design                                               | didn't specifically know how to do architecture | planning poker and divide issues, then work on assigned |
| Bach   | absent                                                                            |                                                 |                                                         |
| Bobby  | requirements, put them in gitlab issue board                                      | none                                            | planning poker and divide issues, then work on assigned |
| Kamila | absent                                                                            |                                                 |                                                         |
| Koray  | functional design document, PoA                                                   | none                                            | work on assigned issues                                 |
| Lisa   | wireframes and their description, table of contents for functional design         | none                                            | planning poker and divide issues, then work on assigned |
| Sasha  | functional design, plan of approach, use cases                                    | none                                            | planning poker and divide issues, then work on assigned |
| Trang  | system design, wireframes and their description, code of conduct, submit in on BB | none                                            | planning poker and divide issues, then work on assigned |

### 07.12.2023

|        | Done                                             | Problems                                | Will do                                       |
|--------|--------------------------------------------------|-----------------------------------------|-----------------------------------------------|
| Are    | database design and set up, some queries         | questions about naming                  | discuss database and finish set up            |
| Bach   | some frontend                                    | questions about creating a branch       | backend                                       |
| Bobby  | backend and frontend for creating testcases      | middlewares                             | discuss the middleware                        |
| Kamila | assigned some issues                             | questions about wireframes and database | discuss questions and start working on issues |
| Koray  | absent                                           |                                         |                                               |
| Lisa   | log in page and header frontend, some middleware | database questions                      | finish the backend for log in part            |
| Sasha  | backend for boards                               | database questions                      | routers                                       |
| Trang  | set up the project                               | database questions                      | finish bug report frontend and backend        |

### 12.12.2023

|        | Done                                              | Problems                                 | Will do                                                          |
|--------|---------------------------------------------------|------------------------------------------|------------------------------------------------------------------|
| Are    | helped during meeting with db set up for everyone | none                                     | page for the admin to see all users list                         |
| Bach   | absent                                            |                                          |                                                                  |
| Bobby  | connected db to testcase, and finished backend    | duration part not working because of db  | merge with Lisa's frontend part of testcase, make the url secure |
| Kamila | testers board view                                | don't know how to work with figma        | finish wireframes and start working on that                      |
| Koray  | absent                                            |                                          |                                                                  |
| Lisa   | finished log in part, did frontend for test case  | merge request for lo in part didn't work | assign myself a new issue and work on that                       |
| Sasha  | connected boards with db, did backend             | id of boards question, no access to db   | merge with Koray's frontend                                      |
| Trang  | finished bugreport front-end                      | merging                                  | merge and connect some parts, next task                          |

Bach, Kamila, Koray - got a warning from the group for being late for more than 15 minutes without a valid reason. Next
time it will be a strike.

### 14.12.2023

|        | Done                                                | Problems                                | Will do                                             |
|--------|-----------------------------------------------------|-----------------------------------------|-----------------------------------------------------|
| Are    | page for seeing all users and editing frontend      | fetch, status codes                     | further on the frontend                             |
| Bach   | finished the part of steps in testcases             | none                                    | merge his part to main                              |
| Bobby  | merge everyone's parts                              | merging had bugs                        | middlewares for user inputs                         |
| Kamila | queries, frontend for tester's page                 | none                                    | merge to main when done                             |
| Koray  | learn svelte, some parts for board page             | lack of svelte knowledge and experience | finish his page frontend and merge                  |
| Lisa   | did some backend for users. log out                 | none                                    | merge with Are and make a page to create a new user |
| Sasha  | fixed database issues, connect backend and frontend | none                                    | finish boards pages, merge                          |
| Trang  | finished bug report, add attachment functionality   | none                                    | will merge all parts when they are done             |

### Sprint 1 task completion

| Name   | Task                                                                 | Result             |
|--------|----------------------------------------------------------------------|--------------------|
| Are    | US-2 Creating database for application                               | Finished           |
| -      | US-29 Admin can view a list of all users                             | Finished           |
| -      | (Shared task with Lisa) US-37 Adding new users to the system         | Finished           |
| Bach   | US-10/11/12 Adding/Editing/Deleting steps in test case               | Finished           |
| -      | US-20 Reflecting progress in steps                                   | Finished           |
| Bobby  | US-6 Adding test case                                                | Finished           |
| -      | (Shared task with Lisa) US-7/8/9 Viewing/Editing/Deleting test case  | Finished           |
| Kamila | US-18 Assigning testers to specific test case                        | Move to Sprint 2   |
| -      | US -10 Testers viewing assigned test cases                           | Move to Sprint 2   |
| -      | US-21 Updating test case based on step status                        | Move to Sprint 2   |
| Koray  | (Shared task with Sasha) US-3 Adding new boards for sprints          | Finished           |
| -      | (Shared task with Sasha) US-4 Viewing existing boards                | Finished           |
| Lisa   | US-1 Different rights for product owner                              | Finished           |
| -      | (Shared task with Are) US-17 Updating user roles in the system       | Finished           |
| -      | US-27 Users commenting on test case                                  | Move to Sprint 2   |
| -      | US-37 Adding new users to the system                                 | Finished           |
| -      | (Shared task with Bobby) US-7/8/9 Viewing/Editing/Deleting test case | Finished           |
| Sasha  | (Shared task with Koray) US-3 Adding new boards for sprints          | Finished           |
| -      | (Shared task with Koray) US-4 Viewing existing boards                | Finished           |
| Trang  | US-31 Set up the project, install libraries                          | Finished           |
| -      | US-22 Detailed bug report for test step                              | Finished           |
| -      | US-24 Modifying bug reports of test steps                            | Move to Sprint 2   |
| -      | US-26 Alerting testers before test case due dates                    | Move to Sprint 2   |
| -      | US-36 Adding attachments to bug report                               | Partially Finished |

## Sprint 2

### 19.12.2023

|        | Done                                                                                       | Problems              | Will do                                               |
|--------|--------------------------------------------------------------------------------------------|-----------------------|-------------------------------------------------------|
| Are    | fixed some frontend to be presentable, dummy data, calls for test cases for specific board | none                  | changes in database, run tests (timer)                |
| Bach   | validation middlewares for steps                                                           | tokens didn't work    | correcting the status of the steps                    |
| Bobby  | validation middlewares for testcases                                                       | none                  | generating link to reset password for users           |
| Kamila | changing the status of the testcase, account page for tester                               | merging had conflicts | visual of the page according to the client's feedback |
| Koray  | absent                                                                                     |                       |                                                       |
| Lisa   | adding new users to the system                                                             | none                  | board filtering                                       |
| Sasha  | fixed database problems on her local                                                       | none                  | board status, updating boards, backend validations    |
| Trang  | view of single board fixed, merging, validation middleware for bug report                  | none                  | attachment, notifications, bug report validation      |

### 21.12.2023

|        | Done                                                                               | Problems                                 | Will do                                     |
|--------|------------------------------------------------------------------------------------|------------------------------------------|---------------------------------------------|
| Are    | updated database, testcase can have multiple products, added due_date of the board | none                                     | start with running test, timer              |
| Bach   | validation of step, adding status to bug report                                    | status in bug report problem with toggle | work on new issue                           |
| Bobby  | absent                                                                             |                                          |                                             |
| Kamila | refactored backend, finished accounts UI                                           | design choices to discuss                | fix the testcases view on the accounts page |
| Koray  | absent                                                                             |                                          |                                             |
| Lisa   | made pages accessible only for logged in users, header reset                       | none                                     | work on board visualisation                 |
| Sasha  | almost finished creating new boards, few design changes                            | none                                     | finish boards view                          |
| Trang  | finished validation for bug report, changed a single board a bit                   | none                                     | overview of bug report to testcase          |

### 09.01.2024

|        | Done                                                                  | Problems      | Will do                                                |
|--------|-----------------------------------------------------------------------|---------------|--------------------------------------------------------|
| Are    | run test functionality                                                | none          | continue the issues                                    |
| Bach   | fixed the connecting step and bug report status, board reports layout | none          | some more styling on a page                            |
| Bobby  | resetting password research                                           | sending email | discuss changes in db                                  |
| Kamila | absent                                                                |               |                                                        |
| Koray  | nothing                                                               |               |                                                        |
| Lisa   | drag and drop functionality                                           | none          | discuss how exactly steps status should change on drop |
| Sasha  | search bar, filters, sorting boards                                   | none          | discuss which other filters can be used                |
| Trang  | bug reports are related to testcases, edit bug report, some frontend  | none          | attachment in db                                       |

### 11.01.2024

|        | Done                                                 | Problems                        | Will do                                                |
|--------|------------------------------------------------------|---------------------------------|--------------------------------------------------------|
| Are    | finished timer for running the testcase              | none                            | discuss the issues and help merging                    |
| Bach   | fixed board report                                   | none                            | fixes on client feedback                               |
| Bobby  | sending emails for password change                   | some bugs                       | frontend for that                                      |
| Kamila | status of step and testcase connected, export to pdf | none                            | export to csv                                          |
| Koray  | some designs for report                              | questions about design decision | finish the design                                      |
| Lisa   | improved editing testcase, minor frontend            | none                            | fix testcase page on client feedback, add new testcase |
| Sasha  | frontend of the board                                | merge conflict                  | implement clients comments on board                    |
| Trang  | attachment uploader, minor frontend                  | none                            | attachment fixes                                       |

### Sprint 2 task completion

| Name   | Task                                                                                  | Result           |
|--------|---------------------------------------------------------------------------------------|------------------|
| Are    | US-28 User uses test timer                                                            | Finished         |
| -      | US-42 Adding extra columns to database                                                | Finished         |
| Bach   | US-49 Validation for step                                                             | Finished         |
| -      | US-41 Bug report, step are connected by status                                        | Finished         |
| -      | (Shared task with Trang) US-46 Adding status to bug report                            | Finished         |
| -      | US-53 Creating page for board report                                                  | Finished         |
| -      | US-55 Adding status to bug report edit                                                | Finished         |
| Bobby  | US-40 Email system for resetting password                                             | Finished         |
| -      | US-39 Resetting password page                                                         | Finished         |
| -      | US-48 Validation for test case                                                        | Move to Sprint 3 |
| Kamila | US-18 Assigning testers to specific test case                                         | Finished         |
| -      | US-19 Testers viewing assigned test cases                                             | Finished         |
| -      | US-21 Updating test case based on step status                                         | Finished         |
| -      | US-52 Front end update for tester viewing assigned tests                              | Finished         |
| -      | US-16 Exporting Regression Test Reports in Various Formats                            | Half Finished    |
| Koray  | US-13 Visual Representation by table of Board Progress by Test Case Weight            | Move to Sprint 3 |
| -      | US-14 Visual Representation of Board Progress by the Time of each testcase            | Move to Sprint 3 |
| -      | US-15 Visual Representation of Board Progress by the number of Bugs in its Test Cases | Move to Sprint 3 |
| Lisa   | US-45 Drag and drop function for the board to dynamically change test status          | Finished         |
| -      | US-51 Splitting pages on tester's and admin's view                                    | Finished         |
| -      | US-56 Fixing bugs with viewing and editing test case                                  | Finished         |
| Sasha  | US-5 Updating Board Details (Name, Due Date, etc.)                                    | Finished         |
| -      | US-47 Validation for board creation                                                   | Finished         |
| -      | US-37 Seeing the Status of the Board                                                  | Finished         |
| Trang  | US-23 Accessing Bug Reports of Specific Test Steps                                    | Finished         |
| -      | US-50 Validation for bug report                                                       | Finished         |
| -      | US-36 Adding attachments to bug report                                                | Finished         |
| -      | US-52 View/Direct to bug reports that belong to a board                               | Finished         |
| -      | US-54 Move overview of bug report to testcase                                         | Finished         |
| -      | (Shared task with Bach) US-46 Adding status to bug report                             | Finished         |
| -      | US-56 Display existing attachments                                                    | Finished         |

## Sprint 3

### 16.01.2024

|        | Done                                                   | Problems                            | Will do                                   |
|--------|--------------------------------------------------------|-------------------------------------|-------------------------------------------|
| Are    | notification, fixed trouble with filtering on due date | none                                | work on a pge                             |
| Bach   | board report, css, step updating changed, alert        | none                                | fix usage of correct product              |
| Bobby  | page to change password                                | none                                | edit users page                           |
| Kamila | export to csv, options for what exactly to export      | none                                | documentation                             |
| Koray  | busy with filters on board report                      | had problem with filter on duration | finish the filters                        |
| Lisa   | adding new testcase page                               | none                                | backend and API for adding new testcase   |
| Sasha  | menu  for boards, duplicate boards, delete, update     | none                                | fix duplication part to make it beautiful |
| Trang  | .env for variables, bug report fixes on frontend       | none                                | delete uploaded attachment                |

### 18.01.2024

|        | Done                                              | Problems       | Will do                        |
|--------|---------------------------------------------------|----------------|--------------------------------|
| Are    | notification about testcases being due            | none           | documentation                  |
| Bach   | nothing                                           | none           | documentation, board report    |
| Bobby  | users page, done with password reset              | none           | select user on a page frontend |
| Kamila | documentation                                     | none           | documentation, validation on   |
| Koray  | fix the filters                                   | merge conflict | fix merge conflict             |
| Lisa   | add testcase works                                | none           | documentation                  |
| Sasha  | delete/edit board, frontend                       | token reset    | duplicating board              |
| Trang  | delete attachment, some documentation, wireframes | none           | documentation                  |

### 23.01.2024

|        | Done                                                    | Problems                 | Will do                         |
|--------|---------------------------------------------------------|--------------------------|---------------------------------|
| Are    | backend clean up                                        | none                     | backend clean up, documentation |
| Bach   | fixed bugs with board report, documentation             | none                     | documentation                   |
| Bobby  | users page, radio button on the left side in users page | backend of creating user | VPS set up                      |
| Kamila | validations on inputted data                            | none                     | documentation                   |
| Koray  | resolved merge problems from last time                  | none                     | documentation                   |
| Lisa   | filters on a board page                                 | none                     | documentation                   |
| Sasha  | finished duplication of boards, fixed some small bugs   | none                     | documentation                   |
| Trang  | fixed deleting testcase, documentation                  | none                     | documentation                   |

### 25.01.2024

|        | Done                                  | Problems                  | Will do                |
|--------|---------------------------------------|---------------------------|------------------------|
| Are    | class diagram for different resources | none                      | prepare for submission |
| Bach   | documentation                         | structure of the document | prepare for submission |
| Bobby  | vps, push changes on users page       | none                      | prepare for submission |
| Kamila | fixed some bugs                       | none                      | prepare for submission |
| Koray  | finished api specification            | none                      | prepare for submission |
| Lisa   | finished api specification            | none                      | prepare for submission |
| Sasha  | flow charts, fixed some merge bugs    | none                      | prepare for submission |
| Trang  | bug fixing                            | none                      | prepare for submission |

### Sprint 3 task completion

| Name   | Task                                                                                  | Result   |
|--------|---------------------------------------------------------------------------------------|----------|
| Are    | US-65 Clean up backend                                                                | Finished |
| -      | US-26 Alerting Testers Before Test Case Due Dates                                     | Finished |
| -      | (Shared task with everyone) US-67 Documentation                                       | Finished |
| Bach   | US-58 Board report fix                                                                | Finished |
| -      | US-62 Fix Step description updating problem                                           | Finished |
| -      | US-64 Changing the use of environment to product                                      | Finished |
| -      | US-67 Documentation                                                                   | Finished |
| Bobby  | US-69 Showing the correct user in the Users page bug fix                              | Finished |
| -      | US-70 Fixing changing password to an existing user                                    | Finished |
| -      | US-71 Updating the Users page for admin                                               | Finished |
| -      | US-48 Validation for test case                                                        | Finished |
| Kamila | US-66 Backend validations                                                             | Finished |
| -      | (Shared task with everyone) US-67 Documentation                                       | Finished |
| Koray  | US-13 Visual Representation by table of Board Progress by Test Case Weight            | Finished |
| -      | US-14 Visual Representation of Board Progress by the Time of each testcase            | Finished |
| -      | US-15 Visual Representation of Board Progress by the number of Bugs in its Test Cases | Finished |
| -      | (Shared task with everyone) US-67 Documentation                                       | Finished |
| Lisa   | US-60 Adding new test cases to the board                                              | Finished |
| -      | US-68 Filter test cases by product on board                                           | Finished |
| -      | US-64 Clean up frontend                                                               | Finished |
| -      | (Shared task with everyone) US-67 Documentation                                       | Finished |
| Sasha  | US-59 Duplicate board                                                                 | Finished |
| -      | US-74 The Admin can delete boards                                                     | Finished |
| -      | (Shared task with everyone) US-67 Documentation                                       | Finished |
| Trang  | US-56 Display existing attachments                                                    | Finished |
| -      | US-72 Fix deleting items violating FK                                                 | Finished |
| -      | US-24 Modifying bug reports of test steps                                             | Finished |
| -      | US-26 Alerting testers before test case due dates                                     | Finished |
| -      | US-36 Adding attachments to bug report                                                | Finished |
| -      | US-61 Create a properties file                                                        | Finished |
| -      | US-63 Adding function to delete unwanted selected attachment                          | Finished |
| -      | (Shared task with everyone) US-67 Documentation                                       | Finished |